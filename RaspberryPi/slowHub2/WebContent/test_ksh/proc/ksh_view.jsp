<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.testKsh.TabKshDao"%>
<%@page import="slowHub.testKsh.TabKshVO"%>
 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	//로그인여부 판단
	String serverRoot = pageContext.getServletContext().getRealPath("/");//weblogic 8.1적용
	String tab_seq = maya.util.Util.null2str(request.getParameter("tab_seq"));
	//out.println("외부 변수 : tab_seq = "+tab_seq);//화면에 출력
	
	TabKshVO tvo = new TabKshVO(); //VO value object : 값들을 담을수 있는 객체 //변수 3개들어있음
	tvo = TabKshDao.getRow(tab_seq);  //인스턴스 값을 쿼리문을 통해 불러와 값을 넣어줍니다.
	
	//out.println("<BR>number : "+tvo.seq);
	//out.println("<BR>number : "+tvo.ccc);
	//out.println("<BR>number : "+tvo.ddd);
		
	
	if(tvo==null){
		out.println("해당 정보를 찾을 수 없습니다.");
		return;
	}
	
		
	HMParser parser = new HMParser();
	int len = parser.setFile(_serverRoot+"/test_ksh/html/view2.html");
    parser.setVar("seq", tvo.seq);
    parser.setVar("ccc", tvo.ccc);
    //System.out.println("ccc>>>>"+tvo.ccc);
    parser.setVar("ddd", tvo.ddd);    
    
        
	/*
    parser.setVar("fMode", fMode);
    parser.setVar("gift_name", gvo.gift_name);
    parser.setVar("event_seq", gvo.event_seq);
    parser.setVar("gift_seq", gvo.gift_seq);
    parser.setVar("mode_label", mode_label);
    parser.setVar("max_application_win_flg_checked", max_application_win_flg_checked);
    parser.setVar("gift_price", ("".equals(gvo.gift_price)||gvo.gift_price==null)?"0":gvo.gift_price);
    parser.setVar("gift_num", ("".equals(gvo.gift_num)||gvo.gift_num==null)?"0":gvo.gift_num);
    parser.setVar("tax_charge", ("".equals(gvo.tax_charge)||gvo.tax_charge==null)?"0":gvo.tax_charge);
    parser.setVar("vat", ("".equals(gvo.vat)||gvo.vat==null)?"0":gvo.vat);
    parser.setVar("gift_image", gvo.gift_image);
    parser.setVar("gift_img_tag", "".equals(gvo.gift_image)?"":"<img width=100 height=100 src='"+_basePath+""+gvo.gift_image+"'>");
    
    parser.setVar("_basePath", _basePath);
    
    //당첨기준 정보
    List<PrizeBaselineVO> pb_list = null;
    pb_list=GiftDao.getGiftPrizeBaselineList(gvo);
	parser.regBlock("prize_baseline_list");//널 리스트 호출
	//반복구 템플릿 변수 치환
	if(pb_list.size()==0){	
		//parser.setBlock("list_null",true);//널 리스트 호출
		//parser.setVar("tot", list.size()+"");//총 리스트 갯수 출력
	}else{
		//각 줄 반복출력
		for(int i=0; i<pb_list.size(); i++){
			parser.setVar("prize_base_seq",pb_list.get(i).prize_base_seq);
			parser.setVar("n_baseline",pb_list.get(i).n_baseline);
			parser.setVar("win_yn",pb_list.get(i).win_yn);
			parser.setVar("win_id",pb_list.get(i).id);
			parser.setBlock("prize_baseline_list",true);//리스트 호출
		}
	}
	
	*/
    parser.clearVars();
	parser.clearBlock();
	String resultStr = parser.getPage();
	out.println(resultStr);
%>
