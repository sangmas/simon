<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%>
<%@page import="slowHub.testLjh.TabLjhDao"%>
<%@page import="slowHub.testLjh.TabLjhVO"%>
<%@page import="java.util.List"%>
<%@page import="maya.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	
	
	//String seq = Util.null2str(request.getParameter("seq"));
	String seq = Util.requestParamter(request, "seq");	//파라미터 값을 받아옴과 동시에 널포인트익셉션 처리를 해줌
	//out.println("외부 변수 : node_id = "+node_id);//화면에 출력
	
	TabLjhVO tvo = new TabLjhVO(); //VO value object : 값들을 담을수 있는 객체
	tvo = TabLjhDao.getRow(seq);	
	if(tvo==null){
		out.println("해당 정보를 찾을 수 없습니다.");
		return;
	}
	
	
	//HMParser parser = new HMParser();
	//int len = parser.setFile(_serverRoot+"/test_ljh/html/test_tab_view.html");
	
	 // parser : html에 있는 내용(파일)을 읽어서 메모리상에 올리고 그 내용들 중에 <@@H[a]@@>모양인 애들을 내가 원하는 값으로 바꿔준다.
	 // parser는 html에서 값을 받아서 jsp에서 뿌려주는 역할을 한다
	String serverRoot = pageContext.getServletContext().getRealPath("/");//weblogic 8.1적용 		//서버 경로 지정(경로의 기준을 지정)
	HMParser parser = new HMParser(_serverRoot+"/test_ljh/html/test_tab_view.html");
    parser.setVar("node_id", tvo.node_id);
    parser.setVar("sensor_type", tvo.sensor_type);
    parser.setVar("value", tvo.value);
    parser.setVar("seq", tvo.seq);
    parser.setVar("time", tvo.time);
	
    
    
    parser.clearVars();							// 남아있는 parser 변수를 없애줌.
	parser.clearBlock();						// 남아있는 parser block를 없애줌.
	String resultStr = parser.getPage();		// 메모리에 있는 것을 실제로 글자로 리턴해준다.
	out.println(resultStr);						// 리턴한 글자를 뿌린다.
%>
