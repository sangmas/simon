<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.testLjh.TabLjhDao"%>
<%@page import="slowHub.testLjh.TabLjhVO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	String serverRoot = pageContext.getServletContext().getRealPath("/");//weblogic 8.1적용
	String fMode = maya.util.Util.null2str(request.getParameter("fMode"));
	String seq = maya.util.Util.null2str(request.getParameter("seq"));
	
	
	TabLjhVO tvo = new TabLjhVO(); //VO value object : 값들을 담을수 있는 객체
	HMParser parser = new HMParser();
	int len = parser.setFile(_serverRoot+"/test_ljh/html/test_form.html");
	if("modify".equals(fMode)){
		tvo = TabLjhDao.getRow(seq);
		if(tvo==null){
			out.println("해당 정보를 찾을 수 없습니다.");
			return;
		}
	    parser.setVar("fMode", "수정");
	    parser.setVar("pMode", "modify");
	    parser.setVar("seq_readonly", "readonly");
	    
	    
	} else {
    	parser.setVar("fMode", "등록");
	    parser.setVar("pMode", "insert");
	    parser.setVar("seq_readonly", "");
	}
    parser.setVar("node_id", tvo.node_id);
    parser.setVar("sensor_type", tvo.sensor_type);
    parser.setVar("value", tvo.value);
    parser.setVar("seq", tvo.seq);
    parser.setVar("time", tvo.time);

    parser.clearVars();
	parser.clearBlock();
	String resultStr = parser.getPage();
	out.println(resultStr);
%>
