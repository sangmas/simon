<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.atmosphere.AtmosphereDao"%>
<%@page import="slowHub.atmosphere.TabAtmosphereVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %>
<%@page import="maya.util.*"%> 
<%@ include file="../../comm/header.jsp" %>
<%
	//String node_seq = maya.util.Util.null2str(request.getParameter("node_seq"));
	//String node_seq = Util.requestParamter(request, "seq");//node_seq를 seq라고 인식하는 코드
	String curr_page = Util.requestParamter(request, "page");
	curr_page="1";
	int startIdx = 1;
	int ipp = 15;
	if(!"".equals(curr_page)){
		startIdx = (Integer.parseInt(curr_page)-1) * ipp+1;
	}
	
	
	
	
	//TabAtmosphereVO tvo = new TabAtmosphereVO(); //VO value object : 값들을 담을수 있는 객체 //변수 17개들어있음
	//tvo = AtmosphereDao.getRow(node_seq);  //인스턴스 값을 쿼리문을 통해 불러와 값을 넣어줍니다.
	
	//if(tvo==null){
	//	out.println("해당 정보를 찾을 수 없습니다.");
	//	return;
	//}
	
	
	String serverRoot = pageContext.getServletContext().getRealPath("/");//0. 파일 상대경로 찾기
	HMParser parser = new HMParser(_serverRoot+"/ksh_ui_ver3/html/slow_ksh_device_state.html");//1. html 파일을 parser로 읽어들임
	//int len = parser.setFile(_serverRoot+"/test_device_ksh/html/device_view2.html");
	parser.regBlock("list");// T 2.1 블럭등록 및 템플릿 변수 치환
	
	List<TabAtmosphereVO> list_tab = new ArrayList<TabAtmosphereVO>();
	//list_tab = TabDao.getList();
	TabAtmosphereVO p_tvo = new TabAtmosphereVO();
	list_tab = AtmosphereDao.getList(p_tvo,startIdx,ipp);
	
	for(int i=0; i < list_tab.size(); i++){
		TabAtmosphereVO tvo = new TabAtmosphereVO();
		tvo = list_tab.get(i);
	    parser.setVar("node_seq", tvo.node_seq); //1. parser의 parser변수 <@@H[ccc]@@>을 원하는 값으로 치환
	    parser.setVar("node_name", tvo.node_name);
	    parser.setVar("co2", tvo.co2);
	    parser.setVar("temp", tvo.temp);
	    parser.setVar("humid", tvo.humid);
	    parser.setVar("lpg", tvo.lpg);
	    parser.setVar("voc", tvo.voc);
	    parser.setVar("cds", tvo.cds);
	    parser.setVar("uv", tvo.uv);
	    parser.setVar("dust", tvo.dust);
	    parser.setVar("ozone", tvo.ozone);
	    parser.setVar("connect_dt", tvo.connect_dt);
	    parser.setVar("communication_type", tvo.communication_type);
	    parser.setBlock("list",true);
	}
	//parser.setVar("fMode", fMode);
    
	parser.clearVars(); //2-1. 남아있는 parser 변수를 없애줌
	parser.clearBlock(); //2-2. 남아있는 parser block를 없애줌 // 아직이해불가
	out.println(parser.getPage()); //99. parser의 내용을 string으로 반환
%>
