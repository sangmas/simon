<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"  isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<h2>접속자가 많습니다.</h2>
<p>
잠시후 재시도 해주세요. <br>
계속적으로 메시지가 발생하면 관리자에게 연락바랍니다.
<br>
<a href="javascript:history.back()">뒤로가기</a>
</p>
<hr>
에러 타입 : <%= exception.getClass().getName() %> 
<%
System.err.println("Error : " + exception.getMessage());
//System.err.println();
%>
</body>
</html>