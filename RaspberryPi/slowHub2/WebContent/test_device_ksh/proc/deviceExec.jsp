<%--
/*
 ***************************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트
 * @desc     : 
 * -----------------------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  -----------------------------------------------------
 * 1.0   2013.07.20    김영돈            최초 프로그램 작성    
 * -----------------------------------------------------------------------------------
 * Copyright(c) 2009 maya ,  All rights reserved.
 ***************************************************************************************
 */
--%>
<%@page import="slowHub.testDevice.TabDeviceDao"%>
<%@page import="slowHub.testDevice.TabDeviceVO"%>
<%@page import="maya.util.MayaFileUp"%>
<%@page import="maya.util.Util"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.oreilly.servlet.MultipartRequest" %>
<%@page import="java.text.*"%>
<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@ include file="../../comm/header.jsp" %>

<%

	request.setCharacterEncoding("UTF-8");
	String unit_seq = maya.util.Util.null2str(request.getParameter("unit_seq"));
	String unit_name = maya.util.Util.null2str(request.getParameter("unit_name"));
	String reg_dt = maya.util.Util.null2str(request.getParameter("reg_dt"));
	String connect_state = maya.util.Util.null2str(request.getParameter("connect_state"));
	String connect_dt = maya.util.Util.null2str(request.getParameter("connect_dt"));
	String unit_identifier = maya.util.Util.null2str(request.getParameter("unit_identifier"));
	String product_code = maya.util.Util.null2str(request.getParameter("product_code"));
	String compatible_device_code = maya.util.Util.null2str(request.getParameter("compatible_device_code"));
	String manufacture_company_name = maya.util.Util.null2str(request.getParameter("manufacture_company_name"));
	String product_name = maya.util.Util.null2str(request.getParameter("product_name"));
	String communication_type = maya.util.Util.null2str(request.getParameter("communication_type"));
	String cert_key = maya.util.Util.null2str(request.getParameter("cert_key"));
	String external_open_flg = maya.util.Util.null2str(request.getParameter("external_open_flg"));
	String install_postion_coord = maya.util.Util.null2str(request.getParameter("install_postion_coord"));
	String install_inout_type = maya.util.Util.null2str(request.getParameter("install_inout_type"));
	String id = maya.util.Util.null2str(request.getParameter("id"));
	String external_cert_key = maya.util.Util.null2str(request.getParameter("external_cert_key"));

	
	String pMode = maya.util.Util.null2str(request.getParameter("pMode"));
	
	TabDeviceVO vo = new TabDeviceVO();
	vo.unit_seq = unit_seq;
	vo.unit_name = unit_name;
	vo.reg_dt = reg_dt;
	vo.connect_state = connect_state;
	vo.connect_dt = connect_dt;
	vo.unit_identifier = unit_identifier;
	vo.product_code = product_code;
	vo.compatible_device_code = compatible_device_code;
	vo.manufacture_company_name = manufacture_company_name;
	vo.product_name = product_name;
	vo.communication_type = communication_type;
	vo.cert_key = cert_key;
	vo.external_open_flg = external_open_flg;
	vo.install_postion_coord = install_postion_coord;
	vo.install_inout_type = install_inout_type;
	vo.id = id;
	vo.external_cert_key = external_cert_key;
	
	//vo.seq = seq;
			//이 객체를 통째로 파라미터로 넘길 수 있다.
	
	System.out.println("모드 :"+pMode);  
	if("insert".equals(pMode)){
		//등록 수행...
		if(TabDeviceDao.insert(vo)>0){
			%>
			<script>
				alert("등록 성공");
				location.href="device_list.jsp";
				//history.back();
			</script>
		<%
		} else {
			%>
			<script>
				alert("등록 실패");
				history.back();
			</script>
		<%
		}
		
	}else if("modify".equals(pMode)){
			//수정 수행...
			if(TabDeviceDao.update(vo)>0){
				%>
				<script>
					alert("수정 성공");
					location.href="device_view.jsp?seq=<%=vo.unit_seq%>";
					//history.back();
				</script>
			<%
			} else {
				%>
				<script>
					alert("수정 실패");
					history.back();
				</script>
			<%
			}	
			
	}else if("delete".equals(pMode)){
		//수정 수행...
		if(TabDeviceDao.delete(vo.unit_seq)>0){
			%>
			<script>
				alert("삭제 성공");
				location.href="device_list.jsp";
				//history.back();
			</script>
		<%
		} else {
			%>
			<script>
				alert("삭제 실패");
				history.back();
			</script>
		<%
		}	
		
	} else {
		%> 
			<script>
				alert("잘못된 접근입니다.");
				//history.back();
			</script>
		<%
		return;
	}
	
	//권한체크
	
	/*
	if(true){
		//한글 체크
		System.out.println("한글파라메터 : "+da.event_name);
		//return;
	}
	//처리 분기 
	if("insert".equals(pMode)){//등록모드
		da.cre_id =  _authBean.sId; 
		EventDao dao = new EventDao();  
		result = dao.insert(da);
		if(result>0){
			_authBean.messageOutReplace("이벤트  등록 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 등록 실패");
		}
		
	} else if("modify".equals(pMode)){
		//기존파일 삭제
		if(null!=mfile.getFileParameter(file_param)){
			EventVO old_da = new EventVO();
			old_da = EventDao.getRow(da.event_seq);
			mfile.delFileReal(request.getRealPath("/")+old_da.thumbnail_image);
		}
		/*
		if(!_authBean.isAdmin()){
			_authBean.messageOutBack("관리자만 접근 가능합니다.");
			return;
		}*/
		/*
		out.println("이벤트 정보 수정중");
		result = EventDao.update(da); 
		if(result>0){
			//ActionBean.actionLog("이벤트 정보 수정", da.event_seq, _authBean.sId,request.getRemoteAddr());
			_authBean.messageOutReplace("이벤트 정보 수정 성공","./eventList.jsp");
		}else{
			_authBean.messageOutBack("이벤트 정보 수정 실패");
		}
	*/
%>
