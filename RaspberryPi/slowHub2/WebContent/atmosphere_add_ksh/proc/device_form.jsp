<%--
/*
 ***********************************************************************************
 * @project  : 이벤트플레이스(2013) 
 * @source   : 이벤트 등록
 * @desc     : 
 *----------------------------------------------------------------------
 * VER      DATE       AUTHOR    DESCRIPTION
 * ---   ----------  ----------  ------------------------------------------
 * 1.0   2013.07.22    김영돈    최초 프로그램 작성       
 * ----------- ----------  -----------------------------------------------
 * Copyright(c) 2010 maya ,  All rights reserved.
 *************************************************************************
 */
--%> 
<%@page import="slowHub.atmosphere.AtmosphereDao"%>
<%@page import="slowHub.atmosphere.TabAtmosphereVO"%>
 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="htmlParser.HMParser" %> 
<%@ include file="../../comm/header.jsp" %>
<%
	//로그인여부 판단
	String serverRoot = pageContext.getServletContext().getRealPath("/");//weblogic 8.1적용
	//어디서 실행되는지 확인하기 위해서
	String fMode = maya.util.Util.null2str(request.getParameter("fMode"));
	String node_seq = maya.util.Util.null2str(request.getParameter("node_seq"));
	
	//out.println("외부 변수 : tab_seq = "+tab_seq);//화면에 출력
	
	TabAtmosphereVO tvo = new TabAtmosphereVO(); //VO value object : 값들을 담을수 있는 객체 //변수 17개들어있음
    		
	HMParser parser = new HMParser(); //HTML에 있는걸 jsp에 뿌려주는 역할
	int len = parser.setFile(_serverRoot+"/atmosphere_add_ksh/html/device_form.html");
	//등록폼을 가지고 수정폼도 사용할 것입니다.
	if("modify".equals(fMode)){
		tvo = AtmosphereDao.getRow(node_seq);  //인스턴스 값을 쿼리문을 통해 불러와 값을 넣어줍니다.
		//out.println("<BR>number : "+tvo.node_seq);
		//out.println("<BR>number : "+tvo.unit_name);
		//out.println("<BR>number : "+tvo.communication_type);
		if(tvo==null){
	out.println("해당 정보를 찾을 수 없습니다.");
	return;
		}  
		parser.setVar("fMode", "수정"); 
		parser.setVar("pMode", "modify");
		parser.setVar("seq_readonly", "readonly");	
	
	} else {
		parser.setVar("fMode", "등록");
		parser.setVar("pMode", "insert");
		parser.setVar("seq_readonly", "");	
		}
	
	parser.setVar("node_seq", tvo.node_seq);
    parser.setVar("node_name", tvo.node_name);
    parser.setVar("co2", tvo.co2);
    parser.setVar("temp", tvo.temp);
    parser.setVar("humid", tvo.humid);
    parser.setVar("lpg", tvo.lpg);
    parser.setVar("voc", tvo.voc);
    parser.setVar("cds", tvo.cds);
    parser.setVar("uv", tvo.uv);
    parser.setVar("dust", tvo.dust);
    parser.setVar("ozone", tvo.ozone);
    parser.setVar("connect_dt", tvo.connect_dt);
    parser.setVar("communication_type", tvo.communication_type); 
   

    parser.clearVars();
	parser.clearBlock();
	String resultStr = parser.getPage();
	out.println(resultStr);
%>
