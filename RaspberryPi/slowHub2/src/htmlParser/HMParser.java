package htmlParser;

import jregex.*; 
import java.io.*; 
import java.util.*;
/*************************************************************************************************
 * 
 * <pre> 
 * <br/> 블럭과 변수에 대한 치환 클래스
 * <br/>
 * <br/>일반 HTML 파일에 일반변수와 블럭 변수에 대한 선언을 해주고
 * <br/>프로그램 구현 부분에서 HTML 과 DB 부분을 분리하여 사용한다.
 * <br/>
 * <br/>일반 변수 타입 : <!-- H[변수명] -->
 * <br/> -> 수정 : 2003-12-15 : 상기 처럼 주석형태로 사용할때에는 html상에서 표시가 되지 않으므로 <@@ 형태로 변경
 * <br/> -> 일반 변수 타입 : <@@H[변수명]@@>
 * <br/>블럭 변수 타입 : <!-- BLOCKBEGIN[블럭명] --> 블럭 내용 <!-- BLOCKEND[블럭명] -->
 * 
 * @version 1.1(2006-01-23)
 * @author 
 * @modify Youndon, Kim
 * </pre>
*************************************************************************************************/
public class HMParser implements jregex.REFlags { 
	private String			VTAG 	= "H";
	private String			BSTAG 	= "BLOCKBEGIN"; 
	private String			BETAG	= "BLOCKEND";
	
	private String		 	sbuffer = "" ;
	private Hashtable		Blocks  = new Hashtable();

	private final String			LastVer	= "v1.1 2006-01-23";

	public HMParser(){
	
	}

	public HMParser(String srcFile){
		try {
			this.setFile(srcFile);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(" HMParser : srcFile not Found :"+srcFile);
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(" HMParser : srcFile not Read :"+srcFile);
		}
		
	}
	
	/*
	 * 파서의 버전및 최종 수정일자를 리턴한다.
	 */
	public String getVer(){
		return this.LastVer;
	}
	
	/****************************************************************************
	 * 디자인 파일을 읽어 들인 후 그 파일 사이즈를 리턴한다.
	 * 
	 * @param 디자인 파일경로(String)
	 * @return 디자인 파일 사이즈(int)
	 ****************************************************************************/
	public int setFile(String filepath) throws IOException,FileNotFoundException {
		//BufferedReader in = new BufferedReader(new FileReader(filepath));
		
		//euc_kr에서 정상적으로 동작하던 한글이 utf-8일경우 깨져서 대체함.:maya : 2009-11-19
		FileInputStream inputFile = new FileInputStream(filepath); 
		InputStreamReader inputStream = new InputStreamReader(inputFile, "UTF-8"); 
	    BufferedReader in = new BufferedReader(inputStream);  
	      
		StringBuffer sbuffer = new StringBuffer(); 
		String lines = "";
		
		while((lines = in.readLine()) != null) {
			sbuffer = sbuffer.append(lines).append("\n");
		}
		this.sbuffer = sbuffer.toString();
		return sbuffer.length();
		
	}
	
	/****************************************************************************
	 * 해당 변수를 값으로 치환한다.
	 * 치환은 디자인 파일내의 모든 변수에 대해서 행해지며,
	 * 한번 치환이 이루어지고 난 후에 변수는 소멸된다.
	 * 
	 * @param ParserSet 해당 변수 명과 치환 될 값
	 * @return 치환된 변수의 수(int)
	 ****************************************************************************/
	// 정규표현식 : <!--\s*HMVAR\[\s*(.*)\s*\]\s*-->
	// 정규표현식 : <@@\s*HMVAR\[\s*(.*)\s*\]\s*@@>
	public int setVar(ParserSet data) {
		StringBuffer sb = new StringBuffer();
		Pattern p=new Pattern("<@@\\s*"+VTAG+"\\[\\s*"+data.key+"\\s*\\]\\s*@@>", IGNORE_CASE+DOTALL );
		Replacer r=p.replacer(data.value);
		int cnt = r.replace(this.sbuffer,sb);
		this.sbuffer = sb.toString(); 
		//temp Printing
		//System.out.println("pattern1 : "+cnt+"<BR>"+data.key);
		return cnt; 
	}  
	
	/****************************************************************************
	 * 해당 변수를 값으로 치환한다.
	 * 치환은 디자인 파일내의 모든 변수에 대해서 행해지며,
	 * 한번 치환이 이루어지고 난 후에 변수는 소멸된다.
	 * 
	 * @param 해당 변수 명(String)과 치환 될 값(String)
	 * @return 치환된 변수의 수(int)
	 ****************************************************************************/
	public int setVar(String key, String value) {
		if(value==null){
			value = "";
		}
		return this.setVar(new ParserSet(key,value));
	}
	public int setVar(String key, int value) {
		return this.setVar(key,String.valueOf(value)); 
	}
	
	
	/****************************************************************************
	 * 해당 변수들을 해당값으로 치환한다.
	 * 치환은 디자인 파일내의 모든 변수에 대해서 행해지며,
	 * 한번 치환이 이루어지고 난 후에 변수는 소멸된다.
	 * 
	 * @param ParserSet으로 이루어진 Vector
	 * @return 치환된 변수의 수(int)
	 ****************************************************************************/
	public int setVars(Vector dataSet) {
		int result = 0;
		for(int i=0 ; i<dataSet.size() ; i++) {
			result = result + this.setVar((ParserSet)dataSet.elementAt(i));
		}
		return result;
	}
	
	
	/****************************************************************************
	 * 사용하고자 하는 블럭 변수에 대한 등록
	 * 블럭변수가 등록되지 않을 경우, 블럭이 통째로 날라갈 수 있음.
	 *
	 * @param 블럭변수 명 (String)
	 * @return 등록 여부 (boolean)
	 ****************************************************************************/
	 //정규표현식 : <!--\s*BLOCKBEGIN\[\s*(.*)\s*\]\s*-->(.*)<!--\s*BLOCKEND\[\s*(.*)\s*\]\s*-->
	public boolean regBlock(String blockName) {
		Pattern p =new Pattern("<!--\\s*"+BSTAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->(.*)<!--\\s*"+BETAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->", IGNORE_CASE+DOTALL );
		Matcher m = p.matcher(sbuffer);
		
		if(m.find()) {
			Blocks.put(blockName, m.group(0));
			return true;
		}
		
		return false;
	}
	
	/****************************************************************************
	 * 블럭을 치환하고, remain 여부에 따라 블럭을 삭제하거나 남겨둔다.
	 *
	 * @param 블럭변수 명(String), 블럭 삭제 여부(boolean)
	 * @return 치환된 블럭 수
	 ****************************************************************************/
	 //정규표현식 : <!--\s*BLOCKBEGIN\[\s*(.*)\s*\]\s*-->(.*)<!--\s*BLOCKEND\[\s*(.*)\s*\]\s*-->
	public boolean setBlock(String blockName, boolean remain) {
		Pattern p =new Pattern("<!--\\s*"+BSTAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->(.*)<!--\\s*"+BETAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->", IGNORE_CASE+DOTALL );
		Matcher m = p.matcher(sbuffer);
		String block = "";
		StringBuffer sb = new StringBuffer();
		
		if(m.find()) {
			block = m.group(1);
		}
		else {
			return false;
		}
		
		int cnt = 0;
		if(remain) {
			Replacer r=p.replacer(block.concat((String)Blocks.get(blockName)));
			cnt = r.replace(this.sbuffer,sb);
			this.sbuffer = sb.toString();
		}
		else {
			Replacer r=p.replacer(block);
			cnt = r.replace(this.sbuffer,sb);
			this.sbuffer = sb.toString();
		}
		
		if(cnt > 0)
			return true;
		else
			return false;

	}
	
	/****************************************************************************
	 * 블럭을 치환하고, 블럭을 삭제한다.
	 *
	 * @param 블럭변수 명(String)
	 * @return 치환된 블럭 수
	 ****************************************************************************/
	public boolean setBlock(String blockName) {
		return this.setBlock(blockName,false);
	}
	
	/****************************************************************************
	 * 디자인 파일내의 블럭 변수를 모두 삭제한다.
	 * getPage를 실행시키기 전에 clearBlock를 실행할 것을 권장.
	 ****************************************************************************/
	public int clearBlock(String blockName) {
		Pattern p =new Pattern("<!--\\s*"+BSTAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->(.*)<!--\\s*"+BETAG+"\\[\\s*"+blockName+"\\s*\\]\\s*-->", IGNORE_CASE+DOTALL );
		
		StringBuffer sb = new StringBuffer();
		
		int cnt = 0;
		Replacer r=p.replacer("");
		cnt = r.replace(this.sbuffer,sb);
		this.sbuffer = sb.toString();
		
		return cnt;
	}
	
	public int clearBlock() {
		Pattern p =new Pattern("<!--\\s*"+BSTAG+"\\[\\s*(\\w*)\\s*\\]\\s*-->", IGNORE_CASE+DOTALL );
		Matcher m = p.matcher(sbuffer);
		int cnt = 0;		
		while(m.find()) {
			//System.out.println(m.group(1));
			cnt += this.clearBlock(m.group(1));
		}
		return cnt;
	}


	/****************************************************************************
	 * 디자인 파일내의 변수를 모두 삭제한다.
	 * getPage를 실행시키기 전에 clearVars를 실행할 것을 권장.
	 ****************************************************************************/
	public int clearVars() {
		StringBuffer sb = new StringBuffer();
//		Pattern p =new Pattern("<@@\\s*"+VTAG+"\\[\\s*(\\w*)\\s*\\]\\s*@@>", IGNORE_CASE+DOTALL );
		//한글치환 지원안됨 \m
		Pattern p =new Pattern("<@@\\s*"+VTAG+"\\[\\s*\\w*\\s*\\]\\s*@@>", IGNORE_CASE+DOTALL );
		Replacer r=p.replacer("");
		int cnt = r.replace(this.sbuffer,sb);
		this.sbuffer = sb.toString();
		//System.out.println("pattern1 : "+cnt+"<BR>"+datakey);
		return cnt; 
		
	}
	
	/****************************************************************************
	 * 최종적인 HTML을 돌려준다.
	 *
	 * @return html String
	 ****************************************************************************/
	public String getPage() {
		return this.sbuffer;
	}
	
	public static void main(String args[]) {
		HMParser parser = new HMParser();
		
		try{
			int len = parser.setFile("test.ihtml");
			parser.regBlock("tail");
			
			Vector parserset = new Vector();
			parserset.add(new ParserSet("hostname","support.invil.org"));
			parserset.add(new ParserSet("title","@@@ this is title @@@"));
			parserset.add(new ParserSet("comments","@@@ this is comment @@@"));
			parserset.add(new ParserSet("date","@@@ this is date @@@"));
			
			parser.setVars(parserset);
			parser.regBlock("tail");
			parser.regBlock("body");
			
			for(int i=0 ; i<3 ; i++) {
				parser.setVar("ttt","@@@ vars of Block @@@" + String.valueOf(i));
				parser.setBlock("tail",true);
			}
			
			parser.setBlock("body");
			parser.clearBlock("body");
			parser.clearBlock("tail");
			
			System.out.println(parser.getPage());
		}
		catch(IOException ioe) {
			System.err.println(ioe);
		}
	}
	
	/****************************************************************************
	 * copyleft HMParser에 대한 정보
	 * 개발자들에게 아무런 제한이 없으며, 수정도 가능함.
	 * 단 상업적 용도로 사용될 경우, 제작자에게 통보하여야 함.
	 *
	 * @return String
	 ****************************************************************************/
	public String ClassInfo() {
		String info = "Copyleft 2002-2009 MAYA \n";
		info.concat("e-Mail : maya999@krpost.net");
		info.concat("개발자들에게 아무런 제한이 없으며, 수정도 가능함.");
		info.concat("단 상업적 용도로 사용될 경우, 제작자에게 통보하여야 함.");
		return info;
	}
}