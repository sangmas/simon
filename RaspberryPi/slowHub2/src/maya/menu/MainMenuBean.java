package maya.menu;

import javax.servlet.http.HttpServletRequest;

/***************************************************************************************
 * <pre>
 * <BR\> ?꾨줈?앺듃 : ?곌뎄臾몄꽌愿?━(2011) 
 * <BR\> ?뚯뒪 : 硫붾돱?몄뀡泥섎━ Bean
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    源?쁺??          理쒖큹 ?꾨줈洹몃옩 ?묒꽦    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class MainMenuBean {  

	private boolean flag;//硫붾돱?몄뀡??泥섏쓬 ?ъ슜?섎뒗吏??щ? 
	private String md0 = "0";
	private String md1 = "0";
	private String md2 = "0"; 
	private String md3 = "0";
	private String md4 = "0";
	private String md5 = "0";
	
	public String menu_name = "";
	
	/**
	 * 硫붾돱?몄뀡 ?꾩껜 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMdFull() {
		String md = "";
		md = getMd0()+getMd1()+getMd2()+getMd3()+getMd4()+getMd5();
		//System.out.println("menumenu3:"+md);
		md = md.replaceAll("0","");
		return md;
	}

	/**
	 * 硫붾돱?몄뀡 1踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd0() {
		if(md0==null||"".equals(md0)) md0="0";
		return md0;
	}

	/**
	 * 硫붾돱?몄뀡 2踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd1() {
		if(md1==null||"".equals(md1)) md1="0";
		return md1;
	}

	/**
	 * 硫붾돱?몄뀡 3踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd2() {
		if(md2==null||"".equals(md2)) md2="0";
		return md2;
	}

	/**
	 * 硫붾돱?몄뀡 4踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd3() {
		if(md3==null||"".equals(md3)) md3="0";
		return md3;
	}

	/**
	 * 硫붾돱?몄뀡 5踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd4() {
		if(md4==null||"".equals(md4)) md4="0";
		return md4;
	}

	/**
	 * 硫붾돱?몄뀡 6踰덉㎏ 媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public String getMd5() {
		if(md5==null||"".equals(md5)) md5="0";
		return md5;
	}

	/**
	 * 硫붾돱?몄뀡 泥섏쓬 ?ъ슜?щ? ?뚮젅洹?議고쉶
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public boolean getFlag() {
		return flag;
	}

	/**
	 * 硫붾돱?몄뀡 1踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd0(String string) {
		md0 = string;
	    flag = true;
	}

	/**
	 * 硫붾돱?몄뀡 2踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd1(String string) {
		md1 = string;
		flag = true;
	}
	
	/**
	 * 硫붾돱?몄뀡 3踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd2(String string) {
		md2 = string;
		flag = true;
	}

	/**
	 * 硫붾돱?몄뀡 4踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd3(String string) {
		md3 = string;
		flag = true;
	}
	
	/**
	 * 硫붾돱?몄뀡 5踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd4(String string) {
		md4 = string;
		flag = true;
	}
	
	/**
	 * 硫붾돱?몄뀡 6踰덉㎏ ??옣
	 * @param string
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd5(String string) {
		md5 = string;
		flag = true;
	}

	
	/**
	 * 硫붾돱瑜??쇨큵吏?젙
	 * @param string0
	 * @param string1
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void setMd(String string0,String string1,String string2,String string3,String string4, String string5 ) {
		//硫붾돱 Default ?명똿 異붽? (2006-01-17)
		if(string0==null || "".equals(string0)){
			string0 = "0"; 
			string1 = "0"; 
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string1==null || "".equals(string1)){
			string1 = "0"; 
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string2==null || "".equals(string2)){
			string2 = "0"; 
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string3==null || "".equals(string3)){
			string3 = "0"; 
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string4==null || "".equals(string4)){
			string4 = "0"; 
			string5 = "0"; 
		}
		if(string5==null || "".equals(string5)){
			string5 = "0"; 
		}
		
		md0 = string0;
		md1 = string1;
		md2 = string2;
		md3 = string3;
		md4 = string4;
		md5 = string5;
		//System.out.println("menumenu2:"+md0+"[1]"+md1+"[2]"+md2+"[3]"+md3+"[4]"+md4+"[5]"+md5);
		flag = true;
	}
	
	/**
	 * request濡?二쇱뼱吏?媛?硫붾돱 Depth瑜?怨꾩궛( ?섏쐞 Depth媛?二쇱뼱吏???딆븯?꾧꼍??珥덇린 ?뗮똿 ) 
	 * @param request
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void calcMenu(HttpServletRequest request){
		String depth0_code = request.getParameter("depth0_code");
		String depth1_code = request.getParameter("depth1_code");
		String depth2_code = request.getParameter("depth2_code");
		String depth3_code = request.getParameter("depth3_code");
		String depth4_code = request.getParameter("depth4_code");
		String depth5_code = request.getParameter("depth5_code");

		this.calcMenu(depth0_code, depth1_code, depth2_code, depth3_code, depth4_code, depth5_code );
	}
	
	/**
	 * 二쇱뼱吏꾨찓?대? ?몄뀡?쇰줈 蹂?꼍
	 * @param mn
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 *
	 */
	public void calcMenu(String mn){
		String[] menu =  {"","","","","","",""};
		for(int i=0; i<5; i++){
			try {
				menu[i+1] = mn.substring(i,i+1);
			} catch (java.lang.StringIndexOutOfBoundsException e){
				menu[i+1] = "";
			}
		}
		//System.out.println("menumenu1:"+menu[1]+"[1]"+menu[2]+"[2]"+menu[3]+"[3]"+menu[4]+"[4]"+menu[5]+"[5]"+menu[6]);
		this.setMd(menu[1],menu[2],menu[3],menu[4],menu[5],menu[6]);
	}
	
	/**
	 * 二쇱뼱吏?硫붾돱瑜??몄뀡?쇰줈 蹂?꼍
	 * @param depth0_code
	 * @param depth1_code
	 * @param depth2_code
	 * @param depth3_code
	 * @param depth4_code
	 * @param depth5_code
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 */
	public void calcMenu(String depth0_code, String depth1_code, String depth2_code, String depth3_code, String depth4_code, String depth5_code ){
		if(depth0_code==null || "null".equals(depth0_code)){
			depth0_code = "0";
		}
		if(depth1_code==null||"null".equals(depth1_code)){
			depth1_code = this.getMd1(); 
		} else if(depth2_code==null||"null".equals(depth2_code)){
				depth2_code = "0";
				depth3_code = "0";
				depth4_code = "0";
				depth5_code = "0";
		} else if(depth3_code==null||"null".equals(depth3_code)){
			depth3_code = "0";
			depth4_code = "0";
			depth5_code = "0";
		} else if(depth4_code==null||"null".equals(depth4_code)){
			depth4_code = "0";
			depth5_code = "0";
		} else if(depth5_code==null||"null".equals(depth5_code)){
			depth5_code = "0";
		}
		if(depth2_code==null||"null".equals(depth2_code)){
			depth2_code = this.getMd2();
		}
		if(depth3_code==null||"null".equals(depth3_code))
			depth3_code = this.getMd3();
		if(depth4_code==null||"null".equals(depth4_code))
			depth4_code = this.getMd4();
		if(depth5_code==null||"null".equals(depth5_code))
			depth5_code = this.getMd5();
		
		
		this.setMd(depth0_code, depth1_code, depth2_code, depth3_code, depth4_code, depth5_code );
	}
	
	/**
	 * 硫붾돱 ?ㅻ퉬寃뚯씠??媛?졇?ㅺ린
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 */
	/*
	public String getMenuNaviString(){
		 MenuStatisticsBean mb = new MenuStatisticsBean();
		 return mb.getMenuNaviString(this.getMdFull());
	}
	*/
	/**
	 * ?대떦 硫붾돱??硫붾돱紐?媛?졇?ㅺ린 
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 源?쁺??
	 */
	/*
	public String getMenuName(){
		 MenuStatisticsBean mb = new MenuStatisticsBean();
		MenuArticle ma  = new  MenuArticle();
		ma = (MenuArticle)mb.getMenu(this.getMdFull());
		 return ma.menu_name;
	}
	*/
	
}
