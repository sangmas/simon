
package maya.util;

import java.io.*;
import java.util.BitSet;

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :   Decoding 을 Encode 화 하는 함수
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class Encoder
{
	/**
	 * 문자열 엔코딩
	 * @param s
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
    public static String encode(String s)
    {
		if(s==null){
			return null;
		}
        byte byte0 = 10;
        StringBuffer stringbuffer = new StringBuffer(s.length());
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(byte0);
        OutputStreamWriter outputstreamwriter = new OutputStreamWriter(bytearrayoutputstream);
        for(int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);
            if(dontNeedEncoding.get(c))
            {
                stringbuffer.append(Integer.toHexString(c));
                continue;
            }
            try
            {
                outputstreamwriter.write(c);
                outputstreamwriter.flush();
            }
            catch(IOException ex)
            {
                bytearrayoutputstream.reset();
                continue;
            }
            byte abyte0[] = bytearrayoutputstream.toByteArray();
            for(int j = 0; j < abyte0.length; j++)
            {
                char c1 = Character.forDigit(abyte0[j] >> 4 & 0xf, 16);
                if(Character.isLetter(c1))
                    c1 -= ' ';
                stringbuffer.append(c1);
                c1 = Character.forDigit(abyte0[j] & 0xf, 16);
                if(Character.isLetter(c1))
                    c1 -= ' ';
                stringbuffer.append(c1);
            }

            bytearrayoutputstream.reset();
        }

        return stringbuffer.toString();
    }
    
    /**
     * 생성자
     */
    public Encoder()
    {
    }

    static BitSet dontNeedEncoding;
    static final int caseDiff = 32;

    static 
    {
        dontNeedEncoding = new BitSet(256);
        for(int i = 97; i <= 122; i++)          // 알파벳 소문자 a ~ z
            dontNeedEncoding.set(i);

        for(int j = 65; j <= 90; j++)           //  알파벳 대문자 A ~ Z
            dontNeedEncoding.set(j);

        for(int k = 48; k <= 57; k++)        //  숫자 0 ~ 9 
            dontNeedEncoding.set(k);

        dontNeedEncoding.set(32);      //  null
        dontNeedEncoding.set(45);      //  -
        dontNeedEncoding.set(95);      //  _
        dontNeedEncoding.set(46);      //  .
        dontNeedEncoding.set(42);      //  *
    }
}