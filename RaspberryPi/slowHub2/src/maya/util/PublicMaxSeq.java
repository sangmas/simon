package maya.util;

import java.sql.SQLException;
import java.util.HashMap;

import com.ibatis.sqlmap.client.SqlMapClient;

public  class PublicMaxSeq {
	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	public static int num = -1;
	public static int cnt=0;
	  
	public static HashMap<String, Integer> hm = new HashMap<String, Integer>();

	/*
	public synchronized static int  getNext(){
		if(num==-1){
			try {
				num = (Integer)sqlMap.queryForObject("action.getMaxAction");
			} catch (SQLException e) {
			}
		} else {
			return ++PublicMaxSeq.num;
		}
		return num;
		
	}
	*/
	/*
	public synchronized static int  getNext1(String queryId){
		int result = 0;
		cnt++;
		hm.get(queryId);
		if(hm.get(queryId)==null){
			try {
				System.out.println(queryId);
				int a = (Integer)sqlMap.queryForObject(queryId);
				//System.out.println(a);
				if(hm == null){
					hm = new HashMap<String,Integer>();
				}
				//System.out.println("hm:"+hm.toString());
				hm.put(queryId,a);
				result = hm.get(queryId);
				//result = hm.put(queryId, (Integer)sqlMap.queryForObject(queryId));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			result = hm.put(queryId,hm.get(queryId)+1);
		}
		//System.out.println(queryId+">>>>cnt:"+cnt+","+result);

		return result;
		
	}

	public synchronized static int  getNext1(String queryId, String seq){
		int result = 0;
		cnt++;
		hm.get(queryId);
		if(hm.get(queryId)==null){
			try {
				if("".equals(seq)){
					result = (Integer)sqlMap.queryForObject(queryId);
				} else {
					result = hm.put(queryId, (Integer)sqlMap.queryForObject(queryId, seq));
				}
				hm.put(queryId,result);

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			result = hm.put(queryId,hm.get(queryId)+1);
		}
		//System.out.println(queryId+">>>>cnt:"+cnt+","+result);

		return result;
		
	}
	*/
	public synchronized static int  getNext(String queryId){
		return getNext(queryId, null);
	}	
	public synchronized static int  getNext(String queryId, String seq){
		String key_name = queryId;
		key_name +="_"+seq;  
		int nextNum = 0;
		if(hm.get(key_name)==null){
			try {
				if(seq==null){
					nextNum = (Integer)sqlMap.queryForObject(queryId);
				}else {
					nextNum = (Integer)sqlMap.queryForObject(queryId, seq);
				}
			} catch (SQLException e) {
				nextNum = 1;//초기화
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//hm.put(queryId, nextNum);
			hm.put(key_name, nextNum);
		} else {
			//hm.put(queryId,hm.get(queryId)+1);
			//nextNum = hm.get(queryId);
			hm.put(key_name,hm.get(key_name)+1); 
			nextNum = hm.get(key_name);
		}
		//System.out.println(queryId+">>>>cnt:"+cnt+","+nextNum);

		return nextNum;
	}
	

	
}
