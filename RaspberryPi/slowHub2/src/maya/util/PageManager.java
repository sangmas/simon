package maya.util;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import maya.util.MakePage;

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 : 공통 리스트 처리 확장 클래스
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class PageManager {

	protected String dbType ="mysql";
	protected String query = "";
    protected String countQuery = "";
    protected String whereQuery = "";
    protected String orderQuery = "";
    
	//페이징 관련 변수
	protected int ipp = 5; //페이지당 표시 게시물 
	public int totalPage =0;
	public String pageStr = "";
	public int currPage = 1; //이전 다음 페이지를 위한 변수
	public String linkUrl = "";
	public String appendUrl = "";
	public int startIndex = 1;//시작게시물 인덱스
	public int endIndex = 0;//끝 게시물 인덱스
	public int totalCount = 0;//총 게시물수
	
	
	/**
	 * 게시물 카운트를 DB를 통해 조회
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getTotalCount() {
		return totalCount;
	}
	
	/**
	 * 페이징 만들기:페이지당 출력갯수 설정
	 * @param currPage 현재페이지
	 * @param linkUrl 링크URL
	 * @param appendUrl 추가할파라메터
	 * @param ipp 페이지당 표시갯수
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void makePage(int currPage,String linkUrl, String appendUrl, int ipp1, int totalCount){
		this.ipp = ipp1;
		this.totalCount = totalCount;
		makePage(currPage,linkUrl, appendUrl);
	}

	/**
	 * 페이징 만들기
	 * @param currPage 현재페이지
	 * @param linkUrl 링크URL
	 * @param appendUrl 추가할파라메터
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void makePage(int currPage,String linkUrl, String appendUrl) {
		this.currPage = currPage;
		this.linkUrl = linkUrl;
		this.appendUrl = appendUrl;

		startIndex += (currPage -1) * this.ipp;
		endIndex = startIndex + ipp; 
		if(endIndex > this.getTotalCount()){
			endIndex =  this.getTotalCount() ;
		}
		MakePage myPage = new MakePage(this.getTotalCount(),this.ipp);
		myPage.makePageIndex(currPage,linkUrl,appendUrl);
		this.totalPage = myPage.getTotPage();
		this.pageStr = myPage.getPageStr();
		System.out.println("getTotalCount : "+getTotalCount() );
		System.out.println("totalPage : "+totalPage );
		System.out.println("pageStr : "+pageStr );
		
		if("oracle".equals(this.dbType)){
			query = " SELECT * FROM ("+query+ ") WHERE snum >= "+startIndex+" AND snum <= "+(startIndex+ipp-1)+" ";//Oracle
		} else if("mysql".equals(this.dbType)) {
			query = " "+query+ " limit "+(startIndex-1)+" , "+ipp+" ";//mysql
		}		

	}
	
	/**
	 * 이전페이지
	 * @param tag
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String nextPageUrl(String tag){
		int nextPage = currPage - 1;
		if((nextPage) > 0){
			tag = "<a href='"+linkUrl+"?page="+nextPage+"&"+appendUrl+"' data-role='button' >"+tag+"</a>";
		}

		return tag;
	}
	
	/**
	 * 다음페이지
	 * @param tag
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String prevPageUrl(String tag){
		int nextPage = currPage + 1;
		if((nextPage) <= this.totalPage){
			tag = "<a href='"+linkUrl+"?page="+nextPage+"&"+appendUrl+"'  data-role='button' >"+tag+"</a>";
		}

		return tag;
	}
	
	/**
	 * 첫 페이지 by JKH
	 * @param tag
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String startPageUrl(String tag){
		int nextPage = 1;
		if(this.currPage != 1){
			tag = "<a href='"+linkUrl+"?page="+nextPage+"&"+appendUrl+"' class='rd'>"+tag+"</a>";
			//String url = ""+linkUrl+"?page="+nextPage+"&"+appendUrl+"";
			//tag = "<div class='pagemove3_btn' onclick=\"location.href='"+url+"'\"><< 이전</div>";

		}
		return tag;
	}
	
	/**
	 * 마지막 페이지 by JKH
	 * @param tag
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String endPageUrl(String tag){
		int nextPage = this.totalPage;
		if(this.currPage != this.totalPage){
			tag = "<a href='"+linkUrl+"?page="+nextPage+"&"+appendUrl+"' class='rd'>"+tag+"</a>";
		}

		return tag;
	}
	
	/**
	 * 페이지 표시 문자열 가져오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public String getPageStr(){	return pageStr;}
	
	/**
	 * 현재페이지 구해오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getCurrPage() { return currPage;}
	
	/**
	 * 전체페이지 구해오기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getTotalPage(){	return totalPage;}

	/**
	 * 페이지당 표시갯수 구하기
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getIpp(){ return ipp;}
	
	/**
	 * 게시물 시작번호
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getStartIndex(){ return startIndex;}
	
	/**
	 * 게시물 끝번호
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public int getEndIndex(){ return endIndex;}
	
	/**
	 * 쿼리생성하기 : 기본조회쿼리 + where쿼리 + 정렬쿼리
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	
}
