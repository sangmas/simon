
package maya.util;
import java.io.File ;

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :   첨부화일 저장시에 디렉토리의 유무확인과 생성, 첨부화일에 삭제를 하기 위한 Bean
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class FileSaverMng {

	/**
	 * 기본생성자
	 * 부모클래스의 변수 초기화 및 인스턴스 변수 초기화
	 */
	public FileSaverMng() {       	    
	}

	/**
	 * 각 변수들을 초기화	 	 
 	 * 첨부화일 경로를 받아 경로의 디렉토리가 존재하는지 확인하는 메소드입니다. 
	 * @param chk_filepath
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
  public boolean exists(String chk_filepath) {
    File physicalPath = new File(chk_filepath) ;
    return physicalPath.exists() ;
  }

  /**
   * 첨부화일 경로와 화일이름을 받아서 화일 존재의 여부를 확인하는 메소드입니다.
   * @param chk_path_name
   * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
  public boolean isFile(String chk_path_name) {
    File physicalPathName = new File(chk_path_name) ;
    return physicalPathName.isFile() ;
  }
  
  /**
	* 첨부화일 경로를 받아 경로의 디렉토리를 생성하는 메소드입니다. 
   * @param mkname
   * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
   */
	public boolean mkdir(String mkname) {
    File physicalmkdic = new File(mkname) ;
    return physicalmkdic.mkdir() ;
 	}
	
	/**
	 * 첨부화일을 삭제하는 메소드입니다.
	 * @param filepath
	 * @param fileName
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void delete(String filepath, String fileName) {
	    File physicalFile = new File(filepath+fileName) ;
	   //physicalFile.
	    physicalFile.delete();
	    physicalFile.renameTo(new File(filepath+fileName+"_del"));
	    /*
	    System.out.println("파일 삭제중....파일유무 ? "+physicalFile.exists() );
		   
	    //System.out.println("파일 삭제중...."+physicalFile.delete() );
	    while(physicalFile.exists()&&physicalFile.delete()){
	    	System.out.println("파일 삭제 실패 잠시후 다시시도");
	    	try {
	    		Thread.sleep(500);
	    	} catch(Exception ex){
	    		System.out.println("파일 삭제 재시도 대기중 에러 발생:"+ex.getMessage());
	    	}
	    }
	   */
	}
	
  /**
   *  Gabage Collection 되기 전에 소거되지 않은 객체들을 메모리에서 제거
   */
  public void finalize() {

  }		
}