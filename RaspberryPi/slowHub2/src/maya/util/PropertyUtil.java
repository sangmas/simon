package maya.util;
import java.io.*;   
import java.util.*;   

/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\>  소스 : 프로퍼티 유틸리티
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/
public class PropertyUtil {
	   // 프로퍼티 파일 저장 경로   
    private String PROPERTIY_FILE = "D:\\sample.properties";   
    private File profile = null;   
    private FileInputStream fis = null;   
    private FileOutputStream fos = null;   
    private Properties pros = null;
    
    /**
     * 생성자
     */
    public PropertyUtil() {   
        this.init();   
    }
    
    /**
     * 생성자
     * @param path
     */
    public PropertyUtil(String path){
    	this.setPropertyFile(path);
    	this.init();
    }
    
	/**
	 *     
	 * 초기화 
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
    public void init() {   
        profile = new File(PROPERTIY_FILE);   
        // 프로퍼티 객체 생성   
        pros = new Properties();   
        try {   
            // 파일이 없다면 새로 만든다.   
            if (!profile.exists()) profile.createNewFile();   
            // 프로퍼티 파일 인 스트림   
            fis = new FileInputStream(profile);     
            // 프로퍼티 파일 아웃 스트림   
            fos = new FileOutputStream(profile);   
            // 프로퍼티 파일을 메모리에 올린다.(파일을 읽어 온다.)   
            pros.load(new BufferedInputStream(fis));   
        } catch (IOException e) {   
            e.printStackTrace();   
        }   
    }   
  
    /**
     * 프로퍼티 파일 경로를 변경 한다.
     * @param path
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
     */
    public void setPropertyFile(String path) {   
        this.PROPERTIY_FILE = path;   
    }   
  
    /**
     * 키(key)로 값(value)을 가져온다.
     * @param key
     * @return
     * @throws IOException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
     */
    public String getProperty(String key) throws IOException {   
        return pros.getProperty(key);   
    }   
  
    /**
     * 키(key)로 값(value)을 저장한다.
     * @param key
     * @param value
     * @throws IOException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
     */
    public void setProperty(String key, String value) throws IOException {   
        pros.setProperty(key, value);   
    }   
  
    /**
     * 프로퍼티 파일에 최종 저장 한다.
     * @throws IOException
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
     */
    public void storeProperty() throws IOException {   
        pros.store(fos, "");   
    }   
  
}
