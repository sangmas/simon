package maya.util;
/***************************************************************************************
 * <pre>
 * <BR\> 프로젝트 : 연구문서관리(2011) 
 * <BR\> 소스 :    달력 처리 Bean
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> VER      DATE       AUTHOR    DESCRIPTION
 * <BR\> ---   ----------  ----------  -----------------------------------------------------
 * <BR\> 1.0   2010.08.18    김영돈           최초 프로그램 작성    
 * <BR\> -----------------------------------------------------------------------------------
 * <BR\> Copyright(c) 2010 ADDLAB ,  All rights reserved.
 ***************************************************************************************
 * </pre>
**/

public class Calender {

  static int nDaysList[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  static String nDaysYoil[] = { "sun", "mon", "tue", "wed", "thu", "fri", "stu" };

  int Year, Month, NDays, StartDay, Week;
  private String Cal[][];

  /**
   * 기본생성자
   * 부모클래스의 변수 초기화 및 인스턴스 변수 초기화
   */
	public Calender() {
		Cal = new String[6][7];
	}

	/**
	 * 년도 정보를 받는다. 
	 * @param seq
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 *
	 */
	public void setYear(int seq) {
		this.Year = seq;
	}

    /**
     *  주 정보를 받는다.
     * @param seq
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
     */
	public void setWeek(int seq) {
		this.Week = seq;
	}

	/**
   	 * 주 정보를 반환한다.
   	 * @return
   	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public int getWeek() {
		return this.Week;
	}
  
	/**
   	 * 월 일자수를 반환한다.
   	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public int getNDays() {
	  return this.NDays;
	}
  
	/**
	 * 시작 위치를 반환한다.
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public int getStartDay() {
		return this.StartDay;
	}

	/**
   	 * 월을 셋팅한다.
   	 * @param seq
   	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public void setMonth(int seq) {
		this.Month = seq;
		NDays = getDays(Year, Month);
		StartDay = getStartDay(Year, Month);
	}
  
	/**
   	 * 해당 년월일의 총 날자를 구한다.
   	 * @param year
     * @param month
     * @return
   	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public int getDays(int year, int month) {
		if(month == 2) {
			if(((year%4 == 0)&&(year%100 != 0))||(year%400 == 0)) {
				return(29);
			}
		}
		return(nDaysList[month-1]);
	}

	/**
     * 해당 년월일의 시작 요일를 구한다.
     * @param year
     * @param month
   	 * @return
   	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
  public int getStartDay(int year, int month) {
    int totalDays;

    totalDays = (year * 365);
    totalDays += (year-1) / 4;
    totalDays -= (year-1) / 100;
    totalDays += (year-1) / 400;

    for(int i=1;i<month;i++) {
      totalDays += getDays(year, i);
    }

    return(totalDays % 7);
  }

  	/**
   	 * 달력 정보를 배열에 저장한다.
   	 * @param year
   	 * @param month
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
  public void printMonth(int year, int month) {
    this.setYear(year);
    this.setMonth(month);
    int i, j=0, column=0;

    for(i=0; i < StartDay; i++) {
      Cal[j][column] = "";
      column++;
    }

    for(i=1; i <= NDays; i++) {
      Cal[j][column] = Util.toString(i);
      column++;

      if(column == 7) {
        j++;
        column = 0;
      }
    }

    setWeek(j);       // 주 정보를 입력
  }
  
  	/**
  	 * 해당 인덱스의 값을 반환
  	 * @param cols
  	 * @param rows
  	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	  public String getValue(int cols, int rows) {
	    return Cal[cols][rows];
	  }
  
	 /**
	 * 해당 인덱스의 요일명을 반환
	 * @param cols
	 * @return
	 * @version : 1.0
	 * @date : 2010. 8. 18.
	 * @Author : 김영돈
	 */
	public String getYoil(int cols) {
	  return this.nDaysYoil[cols];
	}
  
}