package slowHub.testLjh;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;
import slowHub.testTab.TabVO;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TabLjhDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static TabLjhVO getRow(String seq) throws SQLException {
		return (TabLjhVO)sqlMap.queryForObject("testLjh.getTabRow",seq);
	}
	  
	public static int insert(TabLjhVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.seq = Integer.toString(PublicMaxSeq.getNext("testLjh.getMaxTab"));
		return  sqlMap.update("testLjh.insertTab", vo);
	}
	public static int update(TabLjhVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("testLjh.updateTab", vo);
	}
	
	public static int delete(String seq) throws Exception {
		return  sqlMap.update("testLjh.deleteTab", seq);
	}
	public  static List<TabLjhVO> getList(TabLjhVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("testLjh.getTabList", vo, startIdx-1, ipp); 
	}
	
	public  static List<TabLjhVO> getList() throws SQLException {
		return sqlMap.queryForList("testLjh.getTabList");
	}
}
