package slowHub.testTab;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TabDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public  static TabVO getRow(String seq) throws SQLException {
		return (TabVO)sqlMap.queryForObject("tab.getTabRow",seq);
	}
	  
	public static int insert(TabVO vo) throws Exception {
		vo.seq = Integer.toString(PublicMaxSeq.getNext("tab.getMaxTab"));
		return  sqlMap.update("tab.insertTab", vo);
	}
	public static int update(TabVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("tab.updateTab", vo);
	}
	
	public static int delete(String seq) throws Exception {
		return  sqlMap.update("tab.deleteTab", seq);
	}
	
	public  static int getListCnt(TabVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("tab.getTabListCnt", vo);
	}
	
	public  static List<TabVO> getList(TabVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("tab.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static List<TabVO> getList() throws SQLException {
		return sqlMap.queryForList("tab.getTabList");
	}
	
	



	
}
