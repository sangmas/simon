package slowHub.testKsh;

import java.sql.SQLException;  

import slowHub.testTab.TabVO;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TabKshDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static TabKshVO getRow(String seq) throws SQLException {
		return (TabKshVO)sqlMap.queryForObject("testKsh.getTabKshRow",seq);
	}
	
	public static int insert(TabKshVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("testKsh.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testKsh는 sql 네임스페이스				
	}
	public static int update(TabKshVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("testKsh.updateTab", vo);
	}
	public static int delete(String seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("testKsh.deleteTab", seq);
	}
	
	
}
