package slowHub.sensing_value;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;

import com.ibatis.sqlmap.client.SqlMapClient;

public class sensing_valueDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static sensing_valueVO getRow(String seq) throws SQLException {
		return (sensing_valueVO)sqlMap.queryForObject("sensing_value.getTabsensing_valueRow",seq);//??
	}
	
	public static int insert(sensing_valueVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.seq = PublicMaxSeq.getNext("sensing_value.getMaxTab");
		
		return  sqlMap.update("sensing_value.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(sensing_valueVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("sensing_value.updateTab", vo);
	}
	
	public static int delete(int seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("sensing_value.deleteTab", seq);
	}
	
	public static List<sensing_valueVO> getList() throws SQLException {
		return sqlMap.queryForList("sensing_value.getTabList");
	}//method overiding
	
	public static List<sensing_valueVO> getList(sensing_valueVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("sensing_value.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(sensing_valueVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("sensing_value.getTabListCnt", vo);
	}
	
}
