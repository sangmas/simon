package slowHub.testDevice;

import java.sql.SQLException;
import java.util.List;

import maya.util.PublicMaxSeq;
import slowHub.testTab.TabVO;

import com.ibatis.sqlmap.client.SqlMapClient;

public class TabDeviceDao{

	static SqlMapClient sqlMap = mapper.CommIbatisClient.getSqlMapInstance();
	
	public static TabDeviceVO getRow(String unit_seq) throws SQLException {
		return (TabDeviceVO)sqlMap.queryForObject("testDevice.getTabDeviceRow",unit_seq);//??
	}
	
	public static int insert(TabDeviceVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		vo.unit_seq = Integer.toString(PublicMaxSeq.getNext("testDevice.getMaxTab"));
		
		return  sqlMap.update("testDevice.insertTab", vo); //sql 파라미터 받아옴
								//앞에 testksh는 sql 네임스페이스				
	}
	
	public static int update(TabDeviceVO vo) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.update("testDevice.updateTab", vo);
	}
	
	public static int delete(String seq) throws Exception {
		//eventVO.event_seq =sqlMap.queryForObject("event.getMaxEvent").toString(); 
		return  sqlMap.delete("testDevice.deleteTab", seq);
	}
	
	public static List<TabDeviceVO> getList() throws SQLException {
		return sqlMap.queryForList("testDevice.getTabList");
	}//method overiding
	
	public static List<TabDeviceVO> getList(TabDeviceVO vo, int startIdx, int ipp) throws SQLException {
		return sqlMap.queryForList("testDevice.getTabList", vo, startIdx-1, ipp);
	}
	
	public  static int getListCnt(TabDeviceVO vo) throws SQLException {
		return (Integer)sqlMap.queryForObject("testDevice.getTabListCnt", vo);
	}
	
}
