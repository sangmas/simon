package KshTest_anotherver1;
import java.io.IOException;
import java.util.Set;

import se.hirt.w1.Sensor;
import se.hirt.w1.Sensors;

public class KshTest_anotherver1 {	
	public static void main(String args[]) throws InterruptedException, IOException {

		Set<Sensor> sensors = Sensors.getSensors();
		
		System.out.println(String.format("Found %d sensors!", sensors.size()));
		while (true) {
		    for (Sensor sensor : sensors) {
		        System.out.println(String.format("%s(%s):%3.2f%s", sensor.getPhysicalQuantity(), 
		                                     sensor.getID(), sensor.getValue(), sensor.getUnitString()));
		        Thread.sleep(1000);
		    }
		    System.out.println("");
		    System.out.flush();
		}  
    }
}


