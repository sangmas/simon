package KshTest;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.concurrent.TimeUnit;

public class ReadDHT {
		
	int checksum, count = 0;	
	public static int [] dht_val;
	public int ReadDHT22(){
		
		int checkSum = 0;
		dht_val = new int[5]; //initialize??
		
		
		for(int i = 0; i < 40; i++){
			count=0;
			while(StartSignalDHT.kind.pinoutput.getState() == PinState.LOW){
				try {
					TimeUnit.MICROSECONDS.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				count++;
				if(count >= 255)
					return 4; //time out 4
			}
			
			count=0;
			while(StartSignalDHT.kind.pinoutput.getState() == PinState.HIGH){
				try {
					TimeUnit.MICROSECONDS.sleep(1);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				count++;
				if(count >= 255)
					return 5; //time out 5
			}
			
			//load data			
			
			dht_val[i/8] <<=1;
			
			if (count>16)
				dht_val[i/8]|=1;			
		} //for Ends..
		
		checksum = dht_val[0] + dht_val[1] + dht_val[2] + dht_val[3]; 
		
		if (checksum != dht_val[4])
			return 9; //error check sum
		else
			return 0; //pass
	}
}
