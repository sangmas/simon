package KshTest;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

public class PinCreate {
	public GpioPinDigitalOutput pinoutput;
	public GpioPinDigitalInput pininput;
	 
	public PinCreate() {
		GpioController gpio = GpioFactory.getInstance();	
		
	    this.pinoutput = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "DHT22 with LED", PinState.HIGH); //12      
	    this.pininput = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01, PinPullResistance.PULL_DOWN); //12
	    // initial value is Low	    
		
	}

}
