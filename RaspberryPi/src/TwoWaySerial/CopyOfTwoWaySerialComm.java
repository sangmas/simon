package TwoWaySerial;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.FileDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;



public class CopyOfTwoWaySerialComm {
 
  void connect( String portName ) throws Exception {
    CommPortIdentifier portIdentifier = CommPortIdentifier
        .getPortIdentifier( portName );
    if( portIdentifier.isCurrentlyOwned() ) {
      System.out.println( "Error: Port is currently in use" );
    } else {
      int timeout = 2000;
      CommPort commPort = portIdentifier.open( this.getClass().getName(), timeout );
 
      if( commPort instanceof SerialPort ) {
        SerialPort serialPort = ( SerialPort )commPort;
        serialPort.setSerialPortParams(9600 ,
                                        SerialPort.DATABITS_8,
                                        SerialPort.STOPBITS_1,
                                        SerialPort.PARITY_NONE );
 
        InputStream in = serialPort.getInputStream();
        OutputStream out = serialPort.getOutputStream();
 
        ( new Thread( new SerialReader( in ) ) ).start();
        ( new Thread( new SerialWriter( out ) ) ).start();
 
      } else {
        System.out.println( "Error: Only serial ports are handled by this example." );
      }
    }
  }
 
  public static class SerialReader implements Runnable {
 
    InputStream in;
 
    public SerialReader( InputStream in ) {
      this.in = in;
    }
 
    public void run() {      
    	byte[] buffer = new byte[ 1024 ];
      int len = -1;
    
      try {
        while( ( len = this.in.read( buffer ) ) > -1 ) {
          System.out.print( new String( buffer, 0, len ) );
        }  	    	
      } catch( IOException e ) {
        e.printStackTrace();
      } 
    }
  }
 
  public static class SerialWriter implements Runnable {
 
    OutputStream out;
 
    public SerialWriter( OutputStream out ) {
      this.out = out;
    }
    
    boolean state = false;
    int sensorValue = 0;
    static int good = 75;
    static int bad = 130;
    
    public void countCnt(){
    	int on='1';
    	int off='0';      	
    	
    	while(true){
    		
	    	for(int i=0; i<40; i++) {
	    		sensorValue += 5;    		
	    		try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	    		
	    		if(state == false){	    				
	    			if(sensorValue <= good){
	    	    		try {        
	    	    			this.out.write(off); 
	    	    			state = true;
	    	    			System.out.println("sensor value = "+sensorValue);
	    			    } catch( IOException e ) {
	    			        e.printStackTrace();
	    			    } 
	    	    	}
	    		}
	    		else{
	    			if(sensorValue >= bad){    				
	    		    	try {        
	    		    		this.out.write(on); 
	    		    		state = false;
	    		    		System.out.println("sensor value = "+sensorValue);
	    			    } catch( IOException e ) {
	    			        e.printStackTrace();
	    			    }    		    	    	
	    	    	}	
		    			
	    		}    		
	    	}
    		sensorValue = 0;
    	}
    }    
    
    public void run() {     	   	
    	countCnt();    	
    }
  }
 
  public static void main( String[] args ) {
    try {
      ( new CopyOfTwoWaySerialComm() ).connect( "/dev/ttyUSB0" );
    } catch( Exception e ) {
      e.printStackTrace();
    }
  }
}
 
