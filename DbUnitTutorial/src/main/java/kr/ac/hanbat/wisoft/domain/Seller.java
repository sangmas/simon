package kr.ac.hanbat.wisoft.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor @AllArgsConstructor
public class Seller {
	
	@Getter @Setter 
	private String id;
	@Getter @Setter 
	private String name;
	@Getter @Setter 
	private String email;
	
	
}
