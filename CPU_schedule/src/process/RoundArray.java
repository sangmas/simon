package process;

public class RoundArray 
{
	public int arrival = 0;
	public int pNum = 0;
	public int value = 0;
	
	//무조건 다 나눠서 순서대로 배열에 저장 
	//애초에 디바이드 배열을 클래스로 만들어서 우선권, 도착시간 주어지게 쎃어놈
	//24 버스트 시간 4 타임 슬라이스로 나누면 6 이니까 4를 모조리 채워줌 나머지는 따로 보관
	//3 나누기 4는 0 이니까 나머지로 취급하여 배열에 넣어줌
	//도착시간 우선권을 가지고 정렬!	
	
	public static RoundArray[] createArray(int size)
	{
		RoundArray[] ra = new RoundArray[size];  
		//얘를 클래스로 배열껍데기생성 // n의 이유는 ex) 15, 15, 15 가 각 버스트 시간으로 들어왔을때 1, 1, 1 의 공간이 따로필요해서
		for (int i = 0; i < size; i++) {
			ra[i] = new RoundArray();				//생성함
		}	
		
		return ra;
	}
}
