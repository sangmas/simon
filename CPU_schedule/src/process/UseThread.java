package process;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class UseThread extends Thread 
{
	private final ReentrantLock entLock = new ReentrantLock();
	private final Condition readCond=entLock.newCondition();
	private final Condition writeCond=entLock.newCondition();
	//String threadName;
	
	int priority;
	Process p;
	public UseThread(Process pro, int prio)
	{		
		p=pro;
		setPriority(prio);
		priority=getPriority();
	}	
	
	public void run()
	{				
		entLock.lock();
		//if(priority)
		
		System.out.println("\n");
		System.out.println("=========== "+p.name+" 도착===========");
		System.out.println("=========== "+p.burst_sec+" 만큼 실행됨===========");
		System.out.println("priority : "+priority);
		
		try {
			sleep(p.burst_sec * 500); //실제시간처럼 보이게
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally
		{
			entLock.unlock();
		}
		
	}
	
}
