package process;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class ProcessHandler
{
	int n;	
	Scanner in = new Scanner(System.in);
	ArrayList<Process> pro = new ArrayList<Process>();	

	private void input()
	{
		System.out.println("How many processes will you execute? : ");
		n = in.nextInt();    //it stores received value into n
		
		// pro = new Process[n];//there are 'n' processes you can input
		
		/**
		* n으로 받은 숫자만큼 프로세스 객체 생성
		*/
		for(int i=0;i<n;i++)
		{
			Process process = new Process();
			pro.add(process);  
			process.printProcess();	
		}
	}
	
	private void calcTime()
	{
		/**
		* 실행시간 산출구간입니다.
		*/
		double TotalWait = 0;
		System.out.println("\n=========Shortest-Job-First Scehduling's result : =========\n");
		
		pro.get(0).exeTime = pro.get(0).arrival_time;	
		System.out.println("1번 째로 들어온 Process의 실행시작시간 : "+pro.get(0).exeTime);
			
		for(int i=1;i<n;i++)  // pro[0] 은 이미 실행시간을 0으로 위에서 정해 두었음. pro[1]부터 ++
		{
			if((pro.get(i-1).exeTime + pro.get(i-1).burst_sec) < (pro.get(i).arrival_time))
				pro.get(i).exeTime = pro.get(i).arrival_time;
			else if((pro.get(i-1).exeTime + pro.get(i-1).burst_sec) >= (pro.get(i).arrival_time))
				pro.get(i).exeTime = (pro.get(i-1).exeTime + pro.get(i-1).burst_sec);
			else
				System.out.println("error occurred");	
			
			System.out.println((i+1)+"번 째로 들어온 Process의 실행시작시간 : "+pro.get(i).exeTime);
		}

		System.out.println("--------------------------------");
		/**
		* 대기시간을 구하고 평균 대기시간을 구하는 부분입니다.
		*/		
		int i = 1;
		for (Process process : pro)
		{		
			process.waitTime = (process.exeTime - process.arrival_time); //실행시작시간-도착시간=대기시간				
			System.out.println((i++)+"번째로 들어온 Process의 대기시간입니다 : "+process.waitTime);
		}		
		System.out.println("--------------------------------");
		
		for (Process process : pro)
		{
			TotalWait = TotalWait + process.waitTime;
			process.printProcess(); //정렬된 Process 를 뽑아냄.
		}
	
		System.out.println("\n총 평균 대기 시간은?  :   "+TotalWait/n +" ms");

	}
	
	private void threadPrint()
	{
		//스레드로 출력
		for (Process process : pro)
		{
			UseThread thread = new UseThread(process, process.priority);//1이 제일 낮은 우선권임.
			thread.start();
			
			try {
				thread.join();   //프로세스 기다려줌 종료될때까지
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}					
		System.out.println("\n\n");
	}
	
	
	ArrayList<UseThread> ut = new ArrayList<UseThread>();
	
	private void inputThread()
	{		
		for (Process process : pro)
		{
			ut.add(new UseThread(process, process.priority)); 			
		}
		/*
		for(int i=0;i<n;i++)
			System.out.println("Array Therad : "+ut.get(i).priority);
			*/
	}	
	
	public void SJF() 
	{
		input();
		
		Collections.sort(pro, Process.abComparator);		
		
		calcTime();
		
		//선점 프로세스 생성
		System.out.println("사이에 들어올 선점 프로세스를 정보를 입력합니다.");
		Process sunjum = new Process();
		
		//pro.add(sunjum);			
		//Collections.sort(pro, Process.abComparator); 선점으로해보려고 주석처리		
		//threadPrint();
		
		inputThread();
		SunjumThread st = new SunjumThread(sunjum, sunjum.priority);
		
		
		boolean flag = false;
		for(int i=0; i<n; i++)
		{
			if(st.priority > ut.get(i).priority)
			{				
				flag = true;				
			}
		}
		
		if(flag == true)
		{
			st.start();
			try {
				st.join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			for (int j=0;j<n;j++)
			{			
				ut.get(j).start();		
				
				try {
					ut.get(j).join();   //프로세스 기다려줌 종료될때까지				
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}			
		}		
		
		else
		{
			for (int j=0;j<n;j++)
			{			
				ut.get(j).start();		
				
				try {
					ut.get(j).join();   //프로세스 기다려줌 종료될때까지				
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
			
			st.start();
			flag = true;
			try {
				st.join();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		//calcTime();
		
		flag = false;
		
	} //SJF 메서드 끝		
	
	
	public void FCFS()
	{		
		input();
		
		Collections.sort(pro, Process.arrivalComparator);		
		
		System.out.println("=========First Come First Served Scehduling's result : =========\n");
		
		calcTime();
		
		threadPrint();
		
	} //fcfs ends

	public void PriorityScheduling()
	{
		input();
		
		Collections.sort(pro, Process.pabComparator);
		
		calcTime();
		
		threadPrint();
		
	} //priority ends
	
	
	public void RR()
	{					
		int timeSlice;
		int totalBurst = 0;
		
		input();
		
		int n = pro.size(); //프로세스 몇개 만들거니
		
		System.out.println("How much is The Time Slice size? : ");
		timeSlice = in.nextInt();    //Time Quantum.
		
		int[] pMoc = new int[n];  //프로세스 나눌 몫 쓰려고 생성
		for (int i=0;i<n;i++)
		{
			Process process = pro.get(i);
			pMoc[i] = process.burst_sec / timeSlice;			
			if(process.burst_sec % timeSlice > 0)
			{
				pMoc[i]++;
			}	
			totalBurst += pMoc[i];
		}		
		
		// totalBurst= (totalBurst/timeSlice); //버스트시간 다 합쳐서 TimeSlice로 나누어주고 (+ 나머지)
		System.out.println("배열의 길이"+totalBurst);
		
		RoundArray[] ra = RoundArray.createArray(totalBurst);
		
		int cnt=0; //ra 배열 세주기 위한 카운트.. 돌다가 이 카운트 부터 다시 채워넣으려고
		
		for(int i=0;i<n;i++)
		{
			Process process = pro.get(i);			
			int k = process.burst_sec / timeSlice; //ex) 24버스트 시간을 4 타임 슬라이스로 나눈값
			int s=cnt;
		
			for(int j=s;j<k+s;j++)
			{				
				ra[j].value = timeSlice;					
				ra[j].pNum = i+1;  //보기좋게 하려고 1더함
				
				System.out.println("how?   "+ra[j].value);				
				cnt++;		
				System.out.println("how cnt?   " +cnt);
			}
			
			if(0 !=(process.burst_sec % timeSlice))  //이중 이프문
			{
				ra[cnt].value = process.burst_sec % timeSlice;						
				ra[cnt].pNum = i+1;
				
				System.out.println("how namuji?   "+ra[cnt].value);
				cnt++;
				System.out.println("how cnt?   " +cnt);				
			}
		}
		
		System.out.println("\n사용할 배열의 크기 (리얼 크기) : " +cnt);
		System.out.println("---------------------------------");		
								
		for(int i=0;i<cnt;i++)	
		{
			System.out.println("value : "+ra[i].value);			
		}
		for(int i=0;i<cnt;i++)	
		{
			System.out.println("pNum : "+ra[i].pNum);
		}		
		
		///////////Round Robin	
		System.out.println("===============Round Robin Result===============");
			
		int[] theNumReal = new int[n];
				
		theNumReal[0] = 0;
		int sum = 0;
		for(int i=1;i<n;i++)
		{	
			sum += pMoc[i-1];
			theNumReal[i] = sum;
		}
				
		int[] nCount = new int[n];  //pMoc[]
		
		////배열 번호대로 추출하기
		for(int i=0;i<n;i++)
		{									
			System.out.println("test Real Num: "+ theNumReal[i]);
			//theNumReal[i]++;			
		}
		
		//리스트하나만들어서 ㅇㅖ를들어 p1 값이 1이었으면 한번돌고 안돌게 만드는 것
		int c = 0;
		while (c < cnt)
		{	
			for(int j=0;j<n;j++)
			{			
				if(nCount[j] < pMoc[j])
				{
					System.out.println("Process Number :  "+ra[theNumReal[j]].pNum+ "   "
							+ "- Value in TimeSlice  " + ra[theNumReal[j]].value);	
					theNumReal[j]++;
					nCount[j]++;
					c++;		
				}
			}				
		}
		
		/*
			threadPrint();
	    */		
	} //RR ends
	
} //클래스 끝 괄호
