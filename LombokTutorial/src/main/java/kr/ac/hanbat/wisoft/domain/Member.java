package kr.ac.hanbat.wisoft.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Member {
	
	@Getter
	private String id;
	
	@Getter
	private String name;
	
	@Getter
	private String email;	
	
	
}
