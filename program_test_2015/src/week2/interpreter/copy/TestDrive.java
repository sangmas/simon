package week2.interpreter.copy;

import java.util.Scanner;

public class TestDrive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		Register reg = new Register();//생성과함께 변수생성됨	
		int ramCnt=0;
		int temRAM, tem;
		Ram ram = new Ram();
		String temOrderStr ="";
		
		System.out.println("몇번 명령 실행할꺼야? :");
		int times = scanner.nextInt(); //몇번할거니?
		int order100Cnt = 0; //100만나면 카운트시킴
		
		
		
		while(order100Cnt!=times){		//100명령이 입력한 times와 같으면 종료
			System.out.println();
			System.out.println();	
			System.out.println("세자리정수입력 :");	
			/*
			int order = scanner.nextInt();
			
			String orderStr = Integer.toString(order);*/
			String orderStr = scanner.next();
						
				switch(orderStr.charAt(0)){
				
					case '1'://앞부분이 1이면 //종료며 여기까지 입력되어야 한 명령이다
						if(orderStr.charAt(1)=='0' && orderStr.charAt(2)=='0'){
							order100Cnt++;//100 명령이 몇번인지 세준다
							
							reg.divide();
							reg.printReg();
							ram.ramNumber[ramCnt]=orderStr;
							System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
							ramCnt++;
						}
						else
							System.out.println("1로 시작되는 인코딩은 100뿐이므로 입력하신 인코딩은 잘못된 것입니다. 다시입력합니다.");						
					break;
					
					case '2':			
						reg.regNum[orderStr.charAt(1)-48] = orderStr.charAt(2)-48; //캐릭터에서 48을 빼준게 int 값 //아스키코드가 그러함
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '3':
						reg.regNum[orderStr.charAt(1)-48] += orderStr.charAt(2)-48;
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
						
					case '4':
						reg.regNum[orderStr.charAt(1)-48] *= orderStr.charAt(2)-48;
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '5':
						reg.regNum[orderStr.charAt(1)-48] = reg.regNum[orderStr.charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '6':					
						reg.regNum[orderStr.charAt(1)-48] += reg.regNum[orderStr.charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '7':
						reg.regNum[orderStr.charAt(1)-48] *= reg.regNum[orderStr.charAt(2)-48];
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
							
					case '8':						
						temRAM = Integer.parseInt(ram.ramNumber[orderStr.charAt(2)-48]); //892가 들어왓다면 램 2번째 있는 임의의 [세자리 숫자]의 램[세자리숫자] 번호로 이동						
						reg.regNum[orderStr.charAt(1)-48] = Integer.parseInt(ram.ramNumber[temRAM]); //형변환이 지저분하지만 제대로 들어감 
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '9':
						temRAM = Integer.parseInt(ram.ramNumber[orderStr.charAt(2)-48]);
						ram.ramNumber[temRAM]=Integer.toString(reg.regNum[orderStr.charAt(1)-48]);
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
					break;
					
					case '0': //order 앞부분이 0일 경우						
						//temRAM = reg.regNum[orderStr.charAt(2)-48]; // 078이 들어간 경우 8레지스터에 1이 들어있다고 가정할 경우 7레지스터로 이동하고 그 레지스터의 값을 램 번호로 하여 명령실행
						
						reg.divide();
						reg.printReg();
						ram.ramNumber[ramCnt]=orderStr;
						System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
						ramCnt++;
						
						if(0<reg.regNum[orderStr.charAt(2)-48] && reg.regNum[orderStr.charAt(2)-48]<1000){
							tem = reg.regNum[orderStr.charAt(1)-48];//7레지스터가 가지고 있는 값을 임시변수에 넣음 그 수는 9일 것임 (예제에서)							
							temOrderStr = ram.ramNumber[tem]; //7레지스터의 값인 9가 tem 에들어갔으니깐 689 가 들어갈 것임
								//여기 변경소스는 ver2에서
							System.out.println("0명령으로 인해 들어간 명령은"+temOrderStr);
							
							
							switch(temOrderStr.charAt(0)){
							
							case '1'://앞부분이 1이면 //종료며 여기까지 입력되어야 한 명령이다
								if(temOrderStr.charAt(1)=='0' && temOrderStr.charAt(2)=='0'){
									order100Cnt++;//100 명령이 몇번인지 세준다
									
									reg.divide();
									reg.printReg();
									ram.ramNumber[ramCnt]=temOrderStr;
									System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
									ramCnt++;
								}
								else
									System.out.println("1로 시작되는 인코딩은 100뿐이므로 입력하신 인코딩은 잘못된 것입니다. 다시입력합니다.");						
							break;
							
							case '2':			
								reg.regNum[temOrderStr.charAt(1)-48] = temOrderStr.charAt(2)-48; //캐릭터에서 48을 빼준게 int 값 //아스키코드가 그러함
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
							
							case '3':
								reg.regNum[temOrderStr.charAt(1)-48] += temOrderStr.charAt(2)-48;
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
								
							case '4':
								reg.regNum[temOrderStr.charAt(1)-48] *= temOrderStr.charAt(2)-48;
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
							
							case '5':
								reg.regNum[temOrderStr.charAt(1)-48] = reg.regNum[temOrderStr.charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
							
							case '6':					
								reg.regNum[temOrderStr.charAt(1)-48] += reg.regNum[temOrderStr.charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
							
							case '7':
								reg.regNum[temOrderStr.charAt(1)-48] *= reg.regNum[temOrderStr.charAt(2)-48];
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
									
							case '8':						
								temRAM = Integer.parseInt(ram.ramNumber[temOrderStr.charAt(2)-48]); //892가 들어왓다면 램 2번째 있는 임의의 [세자리 숫자]의 램[세자리숫자] 번호로 이동						
								reg.regNum[temOrderStr.charAt(1)-48] = Integer.parseInt(ram.ramNumber[temRAM]); //형변환이 지저분하지만 제대로 들어감 
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;
							
							case '9':
								temRAM = Integer.parseInt(ram.ramNumber[temOrderStr.charAt(2)-48]);
								ram.ramNumber[temRAM]=Integer.toString(reg.regNum[temOrderStr.charAt(1)-48]);
								
								reg.divide();
								reg.printReg();
								ram.ramNumber[ramCnt]=temOrderStr;
								System.out.println("명령뭐가들어갔냐면 : "+ram.ramNumber[ramCnt]);
								ramCnt++;
							break;								
						}							
							
							
							
						} //0번 이프문
							
					break;//0번 break
						
				} //switch 문 종료
		}//while 종료
		
		System.out.println();
		System.out.println("명령횟수가 끝났습니다. 더 이상의 입력은 불가합니다.");		
		System.out.println("램에 기록된 명령들..");
		for(int i=0;i<50;i++)			
			System.out.println(i+" :  "+ram.ramNumber[i]);			
		
	}//main 끝	
	
}
