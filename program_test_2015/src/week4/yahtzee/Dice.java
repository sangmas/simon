package week4.yahtzee;

import java.util.Random;

public class Dice {
	
	int[] dice = new int[5];
	int diceStatus[] = new int[6]; 
	Random random = new Random();
	
	public Dice(){		
		arrange();
		showDiceStatus();
	}
	
	public void rollDice(){ //랜덤 값을 돌려 주사위 5개에 넣음
		/*
		for(int i=0;i<dice.length;i++){			
			dice[i] = random.nextInt(6)+1; // 0~5 까지에 1더함
		}*/		
		dice[0]=2;
		dice[1]=2;
		dice[2]=2;
		dice[3]=2;
		dice[4]=3;		
	}	
	
	public void arrange(){
		
		rollDice();
		for(int i=0;i<dice.length;i++){
			switch(dice[i]){
			case 1:diceStatus[0]+=1;break;				
			case 2:diceStatus[1]+=1;break;			
			case 3:diceStatus[2]+=1;break;
			case 4:diceStatus[3]+=1;break;
			case 5:diceStatus[4]+=1;break;
			case 6:diceStatus[5]+=1;break;
			}//switch
		}//for		
	}
	
	public void showDiceStatus(){	
		System.out.println("◆ 현재 주사위의 상태 정보를 출력합니다.");
		System.out.println("=========================");
		for(int i=0;i<diceStatus.length;i++)
			System.out.print(" ["+(i+1)+"]");
		System.out.println();
		for(int i=0;i<diceStatus.length;i++)
			System.out.print("  "+diceStatus[i]+" ");
		System.out.println();
		System.out.println("=========================");
	}
}
