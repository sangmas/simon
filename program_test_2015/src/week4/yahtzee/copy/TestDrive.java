package week4.yahtzee.copy;

public class TestDrive {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Dice dice = new Dice();
		
		ScoreSheet scoreSheet = new ScoreSheet();
				
		while(true){
			scoreSheet.diceRoll();
			scoreSheet.printScoreTable();
			scoreSheet.judgeScore();		
			scoreSheet.selectScore();
			scoreSheet.printScore();
			if(scoreSheet.gameOver == true){
				break;
			}
		}		
		System.out.println("야찌게임이 끝났습니다.");
		
	}

}
