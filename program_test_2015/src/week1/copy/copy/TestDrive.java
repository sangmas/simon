package week1.copy.copy;

import java.util.Scanner;

public class TestDrive {	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		while(true){						
			System.out.println();	
			System.out.println("1부터 10까지 s 입력, 0부터 99,999,999까지 n 입력 :");			
			int s = scanner.nextInt(), n = scanner.nextInt();									
			if(s<1||s>10||n<0||n>99999999){
				System.out.println("알맞은 수를 입력하세요. 프로그램을 종료합니다.");
				break;			
			}			
			int sero = (2*s)+3;		
			String nString = Integer.toString(n);//스트링으로 처음원소~끝원소까지 순서에 맞는 캐릭터 값을 알아내기 위해 변환		
			Arrange arrange = new Arrange();
			arrange.jorip(nString, s, sero); //각 숫자를 조립..				
		}	//while end			
		scanner.close();
	}	//main end		
}	//class end