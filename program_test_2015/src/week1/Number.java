package week1;

public class Number { //만든 모양들을 조립하여 숫자를 만드는 클래스
	Moyang moyang=new Moyang();
	
	public void zero(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganYangJul(s, sero);
		moyang.gongback135(s);
		moyang.jungganYangJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void one1(int s, int sero){
		moyang.gongback135(s);
		moyang.jungganRightJul(s, sero);
		moyang.gongback135(s);
		moyang.jungganRightJul(s, sero);
		moyang.gongback135(s);
	}
	
	public void two(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganLeftJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void three(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void four(int s, int sero){
		moyang.gongback135(s);
		moyang.jungganYangJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.gongback135(s);
	}
	
	public void five(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganLeftJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void six(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganLeftJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganYangJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void seven(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganRightJul(s, sero);
		moyang.gongback135(s);
		moyang.jungganRightJul(s, sero);
		moyang.gongback135(s);
	}
	
	public void eight(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganYangJul(s, sero);
		moyang.garojul135(s);
		moyang.jungganYangJul(s, sero);
		moyang.garojul135(s);
	}
	
	public void nine(int s, int sero){
		moyang.garojul135(s);
		moyang.jungganYangJul(s, sero);
		moyang.gongback135(s);
		moyang.jungganRightJul(s, sero);
		moyang.garojul135(s);
	}
}
