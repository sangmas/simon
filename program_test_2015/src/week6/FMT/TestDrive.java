package week6.FMT;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
	
public class TestDrive {
	
	static List<String> fmtList;
	static BufferedReader in;
	static BufferedWriter out;
	
	public static void main(String[] args) throws IOException {	
		in = new BufferedReader(new FileReader("C:/Users/simon/Desktop/unix_fmt.txt"));
		out = new BufferedWriter(new FileWriter("C:/Users/simon/Desktop/unix_fmt_copy.txt"));	
		
		createArrayList();			
		showStatus();
		
		String tempWord[] = null;
		int lineLength;
				
		
		for(int i=0;i<fmtList.size()-1;i++){
			lineLength = fmtList.get(i).length();			
			
			if(fmtList.get(i).length() < 72 && fmtList.get(i+1).length() > 0 && fmtList.get(i).length() != 0) { //72줄 보다 작고 다음줄이 0 이상이면
						
				tempWord = fmtList.get(i+1).split(" "); //checked
				for(int k=0;k<tempWord.length;k++)
					System.out.println("뜯어낸 문장 "+tempWord[k]);
				
				System.out.println("나는 테스트용 현재 줄을 나타내는 아이입니다 "+fmtList.get(i));
				
				int cnt=0, j;
				while(cnt !=tempWord.length){
					if(lineLength < 72){
						fmtList.set(i, fmtList.get(i).concat(" "+tempWord[cnt])); //합친걸로 대체해줌
						System.out.println(fmtList.get(i));
						cnt++;	
						j=cnt;
						lineLength = fmtList.get(i).length();
					}else
						break;
				}
				
				//tempWord 남은거 합친 문자열을 만들어야함 (i+1) 다음문장에 남은 문자열 합해서 만들어줌				
				fmtList.set(i+1, "");
				for(j = cnt;j < tempWord.length; j++){					
					fmtList.set(i+1, fmtList.get(i+1).concat(tempWord[j])+" ");	
					System.out.println(fmtList.get(i+1));
				}				
				
				System.out.println("변환된 다음문장 "+fmtList.get(i+1));
				System.out.println("test "+lineLength);									
			}
			
			else if(fmtList.get(i).length() < 72 && fmtList.get(i+1).length() == 0){
				//nothing
			}
			
			else if(fmtList.get(i).length()==0){
				//nothing
			}
			
		}			
		
		showStatus();
		
		
		in.close();
		out.close();
		
	}
	
	private static void createArrayList(){
		fmtList = new ArrayList<String>();
		String tempArray;
		try {
			while((tempArray = in.readLine()) !=null) //List에 한줄 씩 읽은 것 저장			
				fmtList.add(tempArray);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private static void showStatus(){
		System.out.println();
		for(int i=0;i<fmtList.size();i++) //List에 저장된 내용 출력
			System.out.println(fmtList.get(i));
		System.out.println();
		
		int lineLength;
		for(int i=0;i<fmtList.size()-1;i++){
			lineLength = fmtList.get(i).length();
			System.out.println("문자열 라인의 길이는 : "+lineLength);
		}
		System.out.println();System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
	
}

