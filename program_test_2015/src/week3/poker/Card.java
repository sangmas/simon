package week3.poker;

public class Card { //받은 카드를 숫자별로 패별로 분류하는 클래스
	int[] blackCardStatus = new int[13];
	int[] whiteCardStatus = new int[13];
	
	int[] blackCardCDHS = new int[4];
	int[] whiteCardCDHS = new int[4];
	
	boolean blackCardFlush=false; //판단해서 여기넣어놓음
	boolean whiteCardFlush=false;
	
	CardCreate createdCard = new CardCreate(); //생성하면 카드 나눠주고 프린트해줌
	
	public Card(){
		makeStatus();
		showStatus();
	}
	
	public void makeStatus(){	
				
		for(int i=0;i<createdCard.blackCard.length;i++){			
			switch(createdCard.blackCard[i].charAt(0)){
			case '2':
				blackCardStatus[0] += 1;
				break;			
			case '3':
				blackCardStatus[1] += 1;
				break;				
			case '4':
				blackCardStatus[2] += 1;
				break;				
			case '5':
				blackCardStatus[3] += 1;
				break;				
			case '6':
				blackCardStatus[4] += 1;
				break;				
			case '7':
				blackCardStatus[5] += 1;
				break;				
			case '8':
				blackCardStatus[6] += 1;
				break;				
			case '9':
				blackCardStatus[7] += 1;
				break;				
			case 'T':
				blackCardStatus[8] += 1;
				break;				
			case 'J':
				blackCardStatus[9] += 1;
				break;				
			case 'Q':
				blackCardStatus[10] += 1;
				break;				
			case 'K':
				blackCardStatus[11] += 1;
				break;				
			case 'A':
				blackCardStatus[12] += 1;
				break;		
			default:
				System.out.println("you have to check the card");
				break;
			}		
		}
		
		for(int i=0;i<createdCard.whiteCard.length;i++){			
			switch(createdCard.whiteCard[i].charAt(0)){
			case '2':
				whiteCardStatus[0] += 1;
				break;			
			case '3':
				whiteCardStatus[1] += 1;
				break;				
			case '4':
				whiteCardStatus[2] += 1;
				break;				
			case '5':
				whiteCardStatus[3] += 1;
				break;				
			case '6':
				whiteCardStatus[4] += 1;
				break;				
			case '7':
				whiteCardStatus[5] += 1;
				break;				
			case '8':
				whiteCardStatus[6] += 1;
				break;				
			case '9':
				whiteCardStatus[7] += 1;
				break;				
			case 'T':
				whiteCardStatus[8] += 1;
				break;				
			case 'J':
				whiteCardStatus[9] += 1;
				break;				
			case 'Q':
				whiteCardStatus[10] += 1;
				break;				
			case 'K':
				whiteCardStatus[11] += 1;
				break;				
			case 'A':
				whiteCardStatus[12] += 1;
				break;	
			default:
				System.out.println("you have to check the card");
				break;
			}			
		}		
		
		for(int i=0;i<createdCard.blackCard.length;i++){//블랙 카드 문양 판별		
			switch(createdCard.blackCard[i].charAt(1)){
			case 'C':
				blackCardCDHS[0]+=1;
				break;
			case 'D':
				blackCardCDHS[1]+=1;
				break;
			case 'H':
				blackCardCDHS[2]+=1;
				break;
			case 'S':
				blackCardCDHS[3]+=1;
				break;
			default:
				System.out.println("you have to check the card");
				break;
			}
		}
		
		for(int i=0;i<createdCard.whiteCard.length;i++){//와이트 카드 문양 판별		
			switch(createdCard.whiteCard[i].charAt(1)){
			case 'C':
				whiteCardCDHS[0]+=1;
				break;
			case 'D':
				whiteCardCDHS[1]+=1;
				break;
			case 'H':
				whiteCardCDHS[2]+=1;
				break;
			case 'S':
				whiteCardCDHS[3]+=1;
				break;
			default:
				System.out.println("you have to check the card");
				break;
			}
		}
		
		
		
		
	} //make status()
	
	public void showStatus(){ //이게 사실 줄맞춤 해주고 그러려고 복잡해 보이지만 간단한 소스입니다.
		System.out.println();
		System.out.println("◆ black card status 2~A");
		System.out.println();
		for(int i=0;i<blackCardStatus.length;i++){
			if((i+2)>9)
				System.out.print("  "+(i+2));
			else
				System.out.print("   "+(i+2));
		}
		System.out.println();
		//블랙 카드 뭔 숫자인지 판별
		for(int i=0;i<blackCardStatus.length;i++){			
			System.out.print(" | "+blackCardStatus[i]);
		}		
		System.out.println();
		System.out.println();
		System.out.print("       C       D       H       S");
		System.out.println();
		//블랙 카드 뭔 모양들어왔는지 판별 //플러시여부까지 변수에 저장해놓음
		for(int i=0;i<blackCardCDHS.length;i++){
			System.out.print("   |   "+blackCardCDHS[i]);
			if(blackCardCDHS[i]==5)
				blackCardFlush=true;
		}
		System.out.println();
		System.out.println();
		//////////////////////////////////////////////////////////////
		System.out.println("◆ white card status 2~A");
		System.out.println();
		for(int i=0;i<whiteCardStatus.length;i++){
			if((i+2)>9)
				System.out.print("  "+(i+2));
			else
				System.out.print("   "+(i+2));
		}
		System.out.println();		
		//와이트 카드 뭔 숫자인지 판별
		for(int i=0;i<whiteCardStatus.length;i++){
			System.out.print(" | "+whiteCardStatus[i]);
		}
		System.out.println();
		System.out.println();
		System.out.print("       C       D       H       S");
		System.out.println();
		//와이트 카드 뭔 모양인지 판별 //플러시여부까지 변수에 저장해놓음
		for(int i=0;i<whiteCardCDHS.length;i++){
			System.out.print("   |   "+whiteCardCDHS[i]);
			if(whiteCardCDHS[i]==5)
				whiteCardFlush=true;
		}
		System.out.println();
		System.out.println("=================================================================");
	}
	
}
