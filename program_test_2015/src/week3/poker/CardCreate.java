package week3.poker;

import java.util.Scanner;

public class CardCreate { //카드를 입력받아서 나눠주기까지 함
	
	String[] blackCard= new String[5];
	String[] whiteCard= new String[5];	
	Scanner scanner = new Scanner(System.in);
	
	public CardCreate(){
		createCard();
		showCard();
	}
	
	public String makeCard(){	//요렇게 해놓고 가져다 쓰면 편함	
		return scanner.next();
	}
	
	public void createCard(){		
		for(int i=0;i<blackCard.length;i++)
			blackCard[i] = makeCard();
		
		for(int i=0;i<whiteCard.length;i++)
			whiteCard[i] = makeCard();
	}
	
	public void showCard(){
		System.out.println("=================================================================");
		System.out.print("● black team : ");
		for(int i=0;i<(blackCard.length);i++)
			System.out.print(" "+blackCard[i]);
		System.out.println();
		
		System.out.print("● white team : ");
		for(int i=0;i<(whiteCard.length);i++)
			System.out.print(" "+whiteCard[i]);
		System.out.println();
		System.out.println("=================================================================");
	}
}
