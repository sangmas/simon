package week5.searching7num.copy;
public class TestDrive {
	public static void main(String[] args) {	
		
		int magicNumCnt=0;		
		int tempNum[][] = new int[110][3];	
				
		for(int i=0;i<110;i++){ //숫자 배열해줌..
			for(int j=0;j<3;j++){				
				if(j==1){
					tempNum[i][j]=i/10;
					if(tempNum[i][j]>9)
						tempNum[i][j]=tempNum[i][j]/10; //109 까지니까 항상 100 넘어가면 무조건 나머지 - 1 임 [1 0 9] 면 무조건 1-9 란이야기
				}else if(j==2)
					tempNum[i][j]=i%10;				
			}
		}				
		for(int i=0;i<110;i++){
			for(int j=0;j<3;j++){
				if(j==1)
					if(Math.abs(tempNum[i][j+1]-tempNum[i][j])==7)
						magicNumCnt++;
			}
		}		
		System.out.println("the number of magic number is : "+magicNumCnt);		
	}
}