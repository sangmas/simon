var NAVTREE =
[
  [ "program_test_2015", "index.html", [
    [ "패키지", null, [
      [ "패키지", "namespaces.html", "namespaces" ]
    ] ],
    [ "클래스", null, [
      [ "클래스 목록", "annotated.html", "annotated" ],
      [ "클래스 색인", "classes.html", null ],
      [ "클래스 멤버", "functions.html", [
        [ "모두", "functions.html", null ],
        [ "함수", "functions_func.html", null ],
        [ "변수", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "파일들", null, [
      [ "파일 목록", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_arrange_8java.html",
"namespaceweek1_1_1copy_1_1copy.html"
];

var SYNCONMSG = '패널 동기화를 비활성화하기 위해 클릭하십시오';
var SYNCOFFMSG = '패널 동기화를 활성화하기 위해 클릭하십시오';