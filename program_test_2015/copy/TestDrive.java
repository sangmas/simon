package week6.FMT.copy2.copy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
	
public class TestDrive {
	
	static List<String> fmtList;
	static BufferedReader in;
	static BufferedWriter out;
	
	public static void main(String[] args) throws IOException {	
		in = new BufferedReader(new FileReader("C:/Users/simon/Desktop/unix_fmt.txt"));
		out = new BufferedWriter(new FileWriter("C:/Users/simon/Desktop/unix_fmt_copy.txt"));	
		
		createArrayList();			
		showStatus();
		
		String tempWord[] = null;
		int lineLength;
				
		
		for(int i=0;i<fmtList.size()-1;i++){
			lineLength = fmtList.get(i).length();			
			
			tempWord = fmtList.get(i+1).split(" "); //checked
			for(int k=0;k<tempWord.length;k++)
				System.out.println("뜯어낸 문장 "+tempWord[k]);
			System.out.println("나는 테스트용 현재 줄을 나타내는 아이입니다 "+fmtList.get(i));
			
			
			
			
			if(fmtList.get(i).length() <= 72 && fmtList.get(i+1).length() > 0 && fmtList.get(i).length() != 0) { //72줄 보다 작고 다음줄이 0 이상이면
				int cnt=0, j;
								
				while(cnt !=tempWord.length){ //단어가 다 없어질 때까지 더함
					if(lineLength+tempWord[cnt].length() < 72){ //잘라낸 단어 앞부분과 현재 문장의 길이를 더해서 72 보다 작으면
						fmtList.set(i, fmtList.get(i).concat(" "+tempWord[cnt])); //현재 문장 뒤에 단어 앞부분 부터 합침
						System.out.println(fmtList.get(i));
						cnt++;	
						j=cnt;
						lineLength = fmtList.get(i).length();
						
						if(cnt == tempWord.length){				//이 부분은 자른 단어를 다 썼을 경우에 
							fmtList.remove(i+1);				// 그 문장을 지워버리고
							tempWord = fmtList.get(i+1).split(" "); //다음문장을 땡겨와서 다시 단어들을 배열 큐에 넣습니다.
							cnt = 0;								//그리고 cnt를 초기화 시키면 while 문을 재귀적으로 실행하겠죠..
						}
						
					}else 										//더할 단어가 남아도 조건에 부합하지 않으니 탈출
						break;
				}
				
				//tempWord 남은거 합친 문자열을 만들어야함				
				fmtList.set(i+1, "");
				for(j = cnt;j < tempWord.length; j++){					
					fmtList.set(i+1, fmtList.get(i+1).concat(tempWord[j])+" ");	
					System.out.println(fmtList.get(i+1));					
				}					
							
				fmtList.set(i+1, fmtList.get(i+1).trim());
				
				if(fmtList.get(i+1)=="") //우연히 딱 맞아 떨어져서 뒷 문장이 없어질 경우에 ArrayList 값에서 제외해서 뒤에 줄을 땡겨옴
					fmtList.remove(i+1);
				
				System.out.println("변환된 다음문장 "+fmtList.get(i+1));
				System.out.println("test "+lineLength);				
				
			
			}
			
			else if(fmtList.get(i).length() < 72 && fmtList.get(i+1).length() == 0){				
				System.out.println(" 72보다 작고, 다음것이 0 일 경우.. 제목같은거");			
			}
			
			else if(fmtList.get(i).length()==0){				
				System.out.println("내 자신이 0일 때");
			}
			else if(fmtList.get(i).length() > 72 && fmtList.get(i+1).length() > 0){		//72보다 크면  맨뒤에 단어 잘라서 그 다음 문장 앞부분에 넣음	
				tempWord = fmtList.get(i).split(" "); //checked
				int k;
				for(k=0;k<tempWord.length;k++){
					System.out.println("뜯어낸 문장@@@@72보다 클 때 "+tempWord[k]);					
				}				
				System.out.println("@@@@@@마지막 단어... "+tempWord[k-1]);
				fmtList.set(i, fmtList.get(i).replace(tempWord[k-1], ""));////////////////
				System.out.println("왜안돼 "+fmtList.get(i));
				fmtList.set(i+1, tempWord[k-1].concat(" "+fmtList.get(i+1)));
			}
				
			
		}			
		
		showStatus();
		
		for(int i=0;i<fmtList.size();i++){ //		
			out.write(fmtList.get(i));		
			out.newLine();
		}		
		in.close();
		out.close();
		
	}
	
	private static void createArrayList(){
		fmtList = new ArrayList<String>();
		String tempArray;
		try {
			while((tempArray = in.readLine()) !=null) //List에 한줄 씩 읽은 것 저장			
				fmtList.add(tempArray);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	private static void showStatus(){
		System.out.println();
		for(int i=0;i<fmtList.size();i++) //List에 저장된 내용 출력
			System.out.println(fmtList.get(i));
		System.out.println();
		
		int lineLength;
		for(int i=0;i<fmtList.size()-1;i++){
			lineLength = fmtList.get(i).length();
			System.out.println("문자열 라인의 길이는 : "+lineLength);
		}
		System.out.println();System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
	
}

